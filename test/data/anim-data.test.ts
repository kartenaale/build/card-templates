import { AnimData, animDataSlice } from "../../build/gen-hanzi-data/anim-data"

describe('anim data processing', () => {
  const data : AnimData = {
    strokes: [
      'a', 'b', 'c', 'd'
    ],
    medians: [
      [[0,0]],
      [[1,1]],
      [[2,2]],
      [[3,3]]
    ],
    radStrokes: [ 2, 3 ]
  }
  test('slicing', () => {
    expect(animDataSlice(data, 1, 3)).toEqual({
      strokes: [ 'b', 'c' ],
      medians: [
        [[1,1]],
        [[2,2]]
      ],
      radStrokes: [1]
    }) 
  })
})
