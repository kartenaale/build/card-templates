import {
  BopomofoLetterStroke,
  CjkRadicalsSupplementStroke,
  CjkStroke,
  CjkUnifiedIdeographsStroke,
  StrokeType,
  type StrokeTypeMaybeUnclean
} from '../../src-common/stroke-encodings'
import {
  normalizeStrokes,
  normalizeStroke
} from '../../build/gen-hanzi-data/stroke-normalize'
import cnchar from 'cnchar'
import cncharOrder from 'cnchar-order'
import cncharTrad from 'cnchar-trad'
import { readFileSync } from 'fs'

cnchar.use(cncharOrder, cncharTrad)

test('normalize strokes', () => {
  expect(normalizeStrokes([
    StrokeType.HENG,
    CjkStroke.WANGOU,
    BopomofoLetterStroke.SHUZHEPIE,
    BopomofoLetterStroke.PIEZHE,
    CjkUnifiedIdeographsStroke.HENG,
    CjkUnifiedIdeographsStroke.SHU,
    CjkUnifiedIdeographsStroke.DIAN,
    CjkUnifiedIdeographsStroke.PIE,
    CjkUnifiedIdeographsStroke.SHUWANGOU,
    CjkUnifiedIdeographsStroke.HENGGOU,
    CjkUnifiedIdeographsStroke.SHUGOU,
    CjkUnifiedIdeographsStroke.HENGZHEGOU,
    CjkUnifiedIdeographsStroke.HENGZHEZHEZHEGOU,
    CjkRadicalsSupplementStroke.HENGXIEGOU
  ])).toEqual([
    StrokeType.HENG,
    StrokeType.WANGOU,
    StrokeType.SHUZHEPIE,
    StrokeType.PIEZHE,
    StrokeType.HENG,
    StrokeType.SHU,
    StrokeType.DIAN,
    StrokeType.PIE,
    StrokeType.SHUWANGOU,
    StrokeType.HENGGOU,
    StrokeType.SHUGOU,
    StrokeType.HENGZHEGOU,
    StrokeType.HENGZHEZHEZHEGOU,
    StrokeType.HENGXIEGOU
  ])
})

describe(
  'strokes returned from cnchar can be normalized without error',
  () => {
    const hanziPool = readFileSync(
      'build/gen-hanzi-data/pools/simplified/sinologie-simplified.txt',
      { encoding: 'utf-8' }
    ).split('\n').filter(l => l.length > 0)
    const strokeShapes: StrokeTypeMaybeUnclean[] = Array.from(new Set(
      hanziPool.map(h => {
        const result = cnchar.stroke(h, 'order', 'shape')[0]
        if (result !== undefined) {
          return result.map(s => s.split('|')[0] as StrokeTypeMaybeUnclean)
        } else {
          return []
        }
      }).flat()
    ))
    for (const shape of strokeShapes) {
      test(shape, () => {
        expect(normalizeStroke(shape)).not.toBe('')
      })
    }
  }
)
