import { StrokeType } from '../../src-common/stroke-encodings'
import { query } from '../../build/gen-hanzi-data/query'

describe('select characters produce expected output', () => {
  test('木', async () => {
    const result = await query({ char: '木' })
    expect(result.radical).toEqual('木')
    // if the whole thing is the radical, nothing is highlighted
    expect(result.radStrokes).toBeUndefined()
    expect(result.strokeTypes).toEqual([
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.NA
    ])
    expect(result.trad).toBeUndefined()
    expect(result.strokes.length).toBeGreaterThan(0)
    expect(result.medians.length).toBe(result.strokes.length)
    expect(result.strokeTypes.length).toBe(result.strokes.length)
    expect(result.count).toEqual('4+0')
  })
})
