import { StrokeType } from '../../src-common/stroke-encodings'
import { queryStrokeTypes } from '../../build/gen-hanzi-data/stroke-type'
import { cjkRadicals, kangxiRadicals } from './radicals'
import {
  queryPatchedHanziWriterData
} from '../../build/gen-hanzi-data/patched-hanzi-writer'

describe('Kangxi radicals', () => {
  describe('have non-empty strokes', () => {
    for (const radical of kangxiRadicals) {
      test(radical, () => {
        const strokes = queryStrokeTypes(radical)
        expect(strokes.length).toBeGreaterThan(0)
      })
    }
  })
  const radicalsAgainstStrokes = [
    { char: '⼆', strokes: [StrokeType.HENG, StrokeType.HENG] }
  ]
  for (const { char, strokes } of radicalsAgainstStrokes) {
    expect(queryStrokeTypes(char)).toEqual(strokes)
  }
})

describe('CJK Unified Ideograph radicals', () => {
  describe('have non-empty strokes', () => {
    for (const radical of cjkRadicals) {
      test(radical, () => {
        const strokes = queryStrokeTypes(radical)
        expect(strokes.length).toBeGreaterThan(0)
      })
    }
  })
  const radicalsAgainstStrokes = [
    { char: '二', strokes: [StrokeType.HENG, StrokeType.HENG] }
  ]
  for (const { char, strokes } of radicalsAgainstStrokes) {
    expect(queryStrokeTypes(char)).toEqual(strokes)
  }
})

describe('CJK Radicals Supplement radicals', () => {
  // note: for the moment we accept that some do not yet have strokes
  /* describe('have non-empty strokes', () => {
    for (const radical of cjkRadicalsSupplement) {
      test(radical, () => {
        const strokes = queryStrokeTypes(radical)
        expect(strokes.length).toBeGreaterThan(0)
      })
    }
  }) */
  const radicalsAgainstStrokes = [
    { char: '⺅', strokes: [StrokeType.PIE, StrokeType.SHU] }
  ]
  for (const { char, strokes } of radicalsAgainstStrokes) {
    expect(queryStrokeTypes(char)).toEqual(strokes)
  }
})

describe('Traditional characters with grass radical at the start', () => {
  const chars = [
    '葉',
    '蓮',
    '夢',
    '觀',
    '歡',
    '莊',
    '藥',
    '蓋',
    '藝',
    '華',
    '蔭',
    '舊',
    '蘭',
    '蘋',
    '蕩',
    '蔣',
    '蕭',
    '薑'
  ]
  for (const char of chars) {
    test(char, async () => {
      const hanziWriterCount = (await queryPatchedHanziWriterData(char))
        .strokes.length
      expect(queryStrokeTypes(char, hanziWriterCount).slice(0, 4)).toEqual([
        StrokeType.SHU,
        StrokeType.HENG,
        StrokeType.SHU,
        StrokeType.HENG
      ])
    })
  }
})
