import {
  convertCjkToKangxi,
  convertKangxiToCjk,
  isCjkUnifiedIdeograph,
  isKangxiRadical
} from '../../build/gen-hanzi-data/blocks'

describe('recognizing characters in blocks', () => {
  describe('Kangxi radicals', () => {
    const kangxiRadicals = ['⽅']
    for (const radical of kangxiRadicals) {
      test(radical, () => {
        expect(isKangxiRadical(radical)).toBe(true)
      })
    }
  })
  describe('CJK unified ideographs', () => {
    const cjkRadicals = ['方']
    for (const radical of cjkRadicals) {
      test(radical, () => {
        expect(isCjkUnifiedIdeograph(radical)).toBe(true)
      })
    }
  })
})

describe('conversion', () => {
  test('latin characters', () => {
    expect(convertCjkToKangxi('ABC')).toBe('ABC')
    expect(convertKangxiToCjk('ABC')).toBe('ABC')
  })
  const mappings = [
    { kangxi: '⽅', cjk: '方' }
  ]
  describe('Kangxi radical to CJK unified ideograph', () => {
    for (const { kangxi, cjk } of mappings) {
      expect(convertKangxiToCjk(kangxi)).toEqual(cjk)
    }
  })
  describe('CJK unified ideograph to Kangxi radical', () => {
    for (const { kangxi, cjk } of mappings) {
      expect(convertCjkToKangxi(cjk)).toEqual(kangxi)
    }
  })
})
