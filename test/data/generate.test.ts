import { StrokeType } from 'src-common/stroke-encodings'
import { formatJsonp } from '../../build/gen-hanzi-data/generate'
import { query } from '../../build/gen-hanzi-data/query'

describe('select characters produce expected JSONP output', () => {
  test('木', async () => {
    const jsonp = formatJsonp('木', await query({ char: '木' }))
    const paddingBefore = 'hd(\'木\','
    const paddingAfter = ')'
    expect(jsonp.startsWith(paddingBefore)).toBe(true)
    expect(jsonp.endsWith(paddingAfter)).toBe(true)
    const json = jsonp.substring(
      paddingBefore.length,
      jsonp.length - paddingAfter.length
    )
    const expectedPrefix = '{' +
    '"radical":"木",' +
    '"count":"4+0",' +
    `"strokeTypes":["${
      StrokeType.HENG
    }","${
      StrokeType.SHU
    }","${
      StrokeType.PIE
    }","${
      StrokeType.NA
    }"]`
    // the rest is SVG data and medians, don't want to test that here
    expect(json.substring(0, expectedPrefix.length)).toEqual(expectedPrefix)
  })
})
