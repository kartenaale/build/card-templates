import { getHanziData } from '../../src/components/hanzi-data'
import { lookup } from '../../src/components/hanzi-data/lut'

describe('table properties', () => {
  const testCases = [
    { hanzi: '草', prop: 'radicalMeaningDe', expected: 'Gras' },
    { hanzi: '母', prop: 'radicalMeaningDe', expected: 'Mutter' },
    {
      hanzi: '木',
      prop: 'strokeTypeNumbers',
      expected: ['①', '②', '③', '④']
    },
    {
      hanzi: '道',
      prop: 'strokeTypeNumbers',
      expected: [
        '④',
        '③',
        '①',
        '③',
        '②',
        '⑤',
        '①',
        '①',
        '①',
        '④',
        '⑤',
        '④'
      ]
    }
  ]
  for (const { hanzi, prop, expected } of testCases) {
    test(`${prop} of ${hanzi}`, async () => {
      const actual = lookup(
        await getHanziData({ char: hanzi }),
        prop
      )
      expect(actual).toEqual(expected)
    })
  }
})
