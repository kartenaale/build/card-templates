PARCEL := node_modules/.bin/parcel
ESLINT := node_modules/.bin/eslint
JEST := node_modules/.bin/jest
TSC := node_modules/.bin/tsc
JSONP := node_modules/.bin/to-static-jsonp
NODE_BINS := $(PARCEL) $(ESLINT) $(JEST) $(TSC) $(JSONP)

GEN_HANZI_DATA := $(BUILD_PREFIX)build/gen-hanzi-data/run
BUILD_TS_SOURCES := $(shell find build -name '*.ts')
COMPILED_BUILD_BINS := $(GEN_HANZI_DATA)
COMPILED_BUILD_JS := $(patsubst %.ts,$(BUILD_PREFIX)%.js, \
	$(BUILD_TS_SOURCES) $(SRC_COMMON_TS))

TS_SRC += $(BUILD_TS_SOURCES)
MOSTLY_CLEAN += $(COMPILED_BUILD_JS) $(COMPILED_BUILD_BINS)
DEEP_CLEAN_DIRS += node_modules

.PHONY: node
node: $(NODE_BINS) $(COMPILED_BUILD_BINS)

yarn.lock $(NODE_BINS) &: package.json
	yarn install
	touch yarn.lock $(NODE_BINS)

$(COMPILED_BUILD_BINS): %: %.js
# prepend a shebang line and make executable
	echo "#!/usr/bin/env node" > $@
	cat $< >> $@
	chmod +x $@

$(COMPILED_BUILD_JS) &: $(TSC) $(BUILD_TS_SOURCES) $(SRC_COMMON_TS)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	$(TSC) -p build --outDir $(or $(BUILD_PREFIX),.)
	touch $(COMPILED_BUILD_JS)
