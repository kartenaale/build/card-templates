REQUIRED_ON_PATH = yarn grep find sed zip cut patch awk tr
K := $(foreach exec,$(REQUIRED_ON_PATH), \
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

# export env BUILD_PREFIX as . or the empty empty value to build in the project
# root like in CI, otherwise all generated files are in artifacts/
# note that using . and ./ behave the same as the empty string and the filename
# targets will not start with .
BUILD_PREFIX ?= artifacts
# make empty or add trailing slash
BUILD_PREFIX := $(and $(filter-out . ./,$(BUILD_PREFIX)),$(BUILD_PREFIX)/)
# some build subprocesses need this, e.g. the JS proxy for parcel needs to
# know where to load the chardata from
export BUILD_PREFIX
