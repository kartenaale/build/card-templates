HANZI_DATA := $(BUILD_PREFIX)hanzi-data
DATA_TEST_OK_FLAG := $(BUILD_PREFIX).data-test-ok
SRC_DATA_TESTS_TS := $(shell find test/data -type f -name '*.ts' -o -name '*.txt')
SRC_DATA_TEST_ENTRIES := $(filter %.test.ts,$(SRC_DATA_TESTS_TS))

TS_SRC += $(SRC_DATA_TESTS_TS) $(BUILD_TS_SOURCES)
MOSTLY_CLEAN += $(DATA_TEST_OK_FLAG)
MOSTLY_CLEAN_DIRS += $(HANZI_DATA)

# hanzi data is installed here
ANKI_COL_DIR := $(HOME)/.local/share/Anki2/Main
ANKI_COL_WAL := $(ANKI_COL_DIR)/collection.anki2-wal

.PHONY: data
data: $(HANZI_DATA)

.PHONY: test-data
test-data: $(DATA_TEST_OK_FLAG)

# debug only: this will overwrite existing data,
# alternatively delete js files and reimport anki package
.PHONY: install-data
install-data: $(HANZI_DATA)
	$(if $(wildcard $(ANKI_COL_WAL)), $(error Cannot install while Anki is open))
	cp -f $(HANZI_DATA)/*.js $(ANKI_COL_DIR)/collection.media/

$(HANZI_DATA): \
$(GEN_HANZI_DATA) \
$(wildcard build/gen-hanzi-data/pools/simplified/*.txt) \
$(wildcard build/gen-hanzi-data/pools/traditional/*.txt)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	$(GEN_HANZI_DATA) \
	--out $@ \
	$(addprefix --pool , \
		$(wildcard build/gen-hanzi-data/pools/simplified/*.txt) \
		$(wildcard build/gen-hanzi-data/pools/traditional/*.txt))

# no dependency on the data, we want to iterate fast with the tests
$(DATA_TEST_OK_FLAG): $(JEST) $(SRC_DATA_TESTS_TS) $(BUILD_TS_SOURCES) $(SRC_COMMON_TS)
	$(JEST) $(SRC_DATA_TEST_ENTRIES)
	@$(and $(BUILD_PREFIX),mkdir -p $(BUILD_PREFIX))
	touch $(DATA_TEST_OK_FLAG)
