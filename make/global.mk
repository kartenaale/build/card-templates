.PHONY: all
all: check dist

.PHNY: check
check: lint test

.PHONY: test
test: test-data test-html

.PHONY: mostlyclean
mostlyclean:
	rm -f $(MOSTLY_CLEAN)
	rm -rf $(MOSTLY_CLEAN_DIRS)

.PHONY: clean
clean:
	rm -f $(MOSTLY_CLEAN) $(CLEAN)
	rm -rf $(MOSTLY_CLEAN_DIRS) $(CLEAN_DIRS)

# deleted even .venv and node_modules
.PHONY: deepclean
deepclean:
	rm -f $(MOSTLY_CLEAN) $(CLEAN)
	rm -rf $(MOSTLY_CLEAN_DIRS) $(CLEAN_DIRS) $(DEEP_CLEAN_DIRS)

.PHONY: install
install: install-data

.DEFAULT_GOAL := all
