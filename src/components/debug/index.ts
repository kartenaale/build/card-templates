import { error } from './log'

export * from './log'

if (typeof document !== 'undefined') {
  // show severe errors in anki so that we know that they happen
  window.onerror = (evtOrMsgString, source, lineNo, _col, errorObj) => {
    let msg = 'Error: '
    if (typeof errorObj !== 'undefined') {
      msg += `"${msg}"`
    } else if (typeof evtOrMsgString === 'string') {
      msg += `"${msg}"`
    } else {
      msg += '<no message>'
    }
    if (typeof source !== 'undefined') {
      msg += ` at ${source}`
      if (typeof lineNo !== 'undefined') {
        msg += `:${lineNo}`
      }
    }
    error(msg)
  }
}
