/**
 * Checks if the argument is non-empty and contains only characters that are
 * _maybe_ chinese characters.
 *
 * By making this check first, we can skip looking up brackets and punctuation
 * in the Hanzi fields.
 */
export function isMaybeHanzi (candidate: string): boolean {
  return /^[^\s\p{Ps}\p{Pe}\p{Po}\p{Pd}\p{Ll}\p{Lu}\p{Sm}]+$/u.test(candidate)
}
