import HanziWriter from 'hanzi-writer'
import { type GetHanziDataKind, getHanziData } from '../hanzi-data'
import { lookup } from '../hanzi-data/lut'
import { colorFg, colorHighlight, hanziSize } from '../theme'

export interface Strokes {
  /**
   * The container for the strokes, initially empty. If `appendTo` was
   * specified during creation, then this container is contained within.
   * Otherwise it is up to the caller to connect this element to the DOM
   * somewhere.
   */
  container: HTMLDivElement
}

export interface StrokesOpts {
  /**
   * A container to use. If not specified, the container for the strokes is
   * returned without adding it to the DOM.
   */
  appendTo: HTMLElement | undefined | null
  /**
   * A single hanzi for which to get the strokes.
   */
  character: string
  kind: GetHanziDataKind
}

/**
 * Returns a container for the strokes of a given character. The container
 * initially has the classes `strokes is-loading`. SVG children for the strokes
 * will be added as they are loading in the background.
 *
 * When `appendTo` is specified, the strokes container will be added to the end
 * of that element.
 *
 * The rendered result looks something like this when fully loaded:
 * ```
 * <div class="strokes">
 *   <svg class="stroke"><!-- stroke 1 --></svg>
 *   <svg class="stroke"><!-- stroke 2 --></svg>
 *   <!-- and more... -->
 * </div>
 *
 * If an error occurs loading the character data, the container is left empty.
 * ```
 */
export async function createStrokes (
  { appendTo, character, kind }: StrokesOpts
): Promise<Strokes> {
  const container = document.createElement('div')
  container.classList.add('strokes')
  if (appendTo != null) {
    appendTo.appendChild(container)
  }

  const data = await getHanziData({
    char: character,
    kind
  })
  if (data !== undefined) {
    const strokeNames = lookup(data, 'strokeTypeNames')
    const strokeNumbers = lookup(data, 'strokeTypeNumbers')
    const { strokes, strokeTypes } = data
    if (
      Array.isArray(strokeNames) &&
        Array.isArray(strokeNumbers) &&
        strokeNames.length === strokeNumbers.length) {
      for (let idx = 0; idx < strokes.length; ++idx) {
        const svg = strokeRangeSvg(strokes, idx + 1)
        const strokeLabel = document.createElement('div')
        if (strokes.length === strokeNames.length) {
          strokeLabel.innerHTML =
              `${
                strokeNames[idx]
              }<br>${
                strokeTypes[idx]
              } ${
                strokeNumbers[idx]
              }<br>${idx + 1}.`
        } else {
          strokeLabel.textContent = `${idx + 1}`
        }
        const strokeInfo = document.createElement('div')
        strokeInfo.classList.add('stroke')
        strokeInfo.appendChild(svg)
        strokeInfo.appendChild(strokeLabel)
        container.appendChild(strokeInfo)
      }
    }
  }

  return { container }
}

function strokeRangeSvg (strokes: string[], untilIdxExcl: number): SVGElement {
  const svg = document.createElementNS(
    'http://www.w3.org/2000/svg', 'svg'
  )
  svg.classList.add('stroke-image')
  svg.style.width = `${hanziSize}px`
  svg.style.height = `${hanziSize}px`
  const group = document.createElementNS(
    'http://www.w3.org/2000/svg', 'g'
  )
  const { transform } = HanziWriter.getScalingTransform(
    hanziSize, hanziSize
  )
  group.setAttributeNS(null, 'transform', transform)
  svg.appendChild(group)
  strokes.slice(0, untilIdxExcl).forEach((strokePath, idx) => {
    const path = document.createElementNS(
      'http://www.w3.org/2000/svg', 'path'
    )
    path.setAttributeNS(null, 'd', strokePath)
    const isLast = (idx + 1) === untilIdxExcl
    path.style.fill = isLast ? colorHighlight : colorFg
    group.appendChild(path)
  })
  return svg
}
