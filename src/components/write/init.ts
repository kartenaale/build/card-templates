import { GetHanziDataKind } from '../hanzi-data'
import { isAnswer } from '../is-q-or-a'
import type { WriterOpts } from './write'
import { createWriter } from './write'

const writerClass = 'strichfolge-animation'

export async function init (within: HTMLElement): Promise<void> {
  const writerContainers: HTMLElement[] = Array.prototype.filter.call(
    within.getElementsByClassName(writerClass),
    (writer: HTMLElement) => {
      return !(
        writer.classList.contains('is-initialized') ||
        writer.classList.contains('is-loading')
      )
    }
  )
  writerContainers.forEach(w => { w.classList.add('is-loading') })
  await Promise.all(writerContainers.map(async writerContainer => {
    const noAnimate = writerContainer.getAttribute('no-animate') !== null
    const strokeNums = writerContainer.getAttribute('animate-stroke')
    const highlightRadicalAttr = writerContainer
      .getAttribute('highlight-radical')
    const highlightRadical = highlightRadicalAttr === 'true' ||
      (highlightRadicalAttr === 'answer' && isAnswer())
    const text = (writerContainer.textContent ?? '').trim()
    const opts: WriterOpts = {
      addTo: writerContainer,
      text,
      isLarge: writerContainer.classList.contains('is-large'),
      kind: writerContainer.classList.contains('is-traditional')
        ? GetHanziDataKind.TRADITIONAL
        : GetHanziDataKind.ANY
    }
    if (strokeNums !== null) {
      const animateStrokeIndexes = strokeNums.split(/\s+/)
        // count from 0 in attribute
        .map(n => Number.parseInt(n) - 1)
      if (typeof opts.animate === 'undefined') {
        opts.animate = {}
      }
      opts.animate.strokes = animateStrokeIndexes
    }
    if (noAnimate) {
      if (typeof opts.animate === 'undefined') {
        opts.animate = {}
      }
      opts.animate.no = true
    }
    if (highlightRadical) {
      opts.highlightRadical = true
    }
    writerContainer.replaceChildren() // clear the text we just retrieved
    if (text.length > 0) {
      await createWriter(opts)
    }
    writerContainer.classList.add('is-initialized')
    writerContainer.classList.remove('is-loading')
  }))
}
