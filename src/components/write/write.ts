import HanziWriter, { type HanziWriterOptions } from 'hanzi-writer'
import { type Strokes, createStrokes } from './strokes'
import {
  colorInactive,
  colorFg,
  colorHighlight,
  hanziSize,
  hanziSizeLarge
} from '../theme'
import { type GetHanziDataKind, getHanziData } from '../hanzi-data'
import { isMaybeHanzi } from '../is-hanzi'
import { error } from '../debug/log'

/** Pause between strokes */
const delayBetweenStrokes = 300
/**
 * Pause before starting the animation again from the start.
 *
 * When looping with multiple characters, we only pause after the last one.
 */
const keepFullCharacterFor = 1500

export interface WriterOpts {
  /**
   * Append the component to the end of this container.
   */
  addTo: Element
  /**
   * Text with chinese characters in it. A Hanzi writer will be created for
   * each non-ascii character except `，` (full-width comma). Commas of this
   * kind as well as the standard ASCII comma and spaces will be rendered as
   * simple text without animation.
   */
  text: string
  /**
   * Optional animation settings, e.g. what stroke to animate.
   */
  animate?: AnimateOpts
  /**
   * If set to true, the radical is shown in a different color than the rest.
   */
  highlightRadical?: true
  isLarge?: boolean
  kind: GetHanziDataKind
}

const writerOptsDefaults = {
  animate: {}
}

const enum State {
  Loading = 'loading',
  Error = 'error',
  Ready = 'ready'
}

interface Write {
  state: State
}

interface AnimateOpts {
  no?: true
  /**
   * For each character in the text, a single stroke to animate.
   */
  strokes?: number[]
}

interface WriteReady extends Write {
  state: State.Ready
  writers: Writer[]
  /**
   * When selecting a writer to show individual strokes, holds the writer for
   * which this is done. `undefined` if no selection.
   */
  selectedWriter: Writer | undefined
  animatedIdx: number
  animateNextIdx: number
  /**
   * 1 when looping through characters, 0 when only animating the one that is
   * currently selected.
   *
   * Assumed to be zero or positive, otherwise behavior is undefined.
   */
  animateNextIncrement: number
  isAnimating: boolean
  /**
   * If true, a writer was selected to be next to animate, and we don't
   * want the usual pause between animations.
   */
  continuationRequested: boolean
}

interface Writer {
  /** Contains the other elements */
  container: HTMLDivElement
  /** The character used by both the writer and the strokes. */
  character: string
  /** A single tile with an animation of all the strokes for the character */
  hanziWriter: HanziWriter
  /** The strokes as individual SVG tiles. */
  strokes: Strokes
}

/// Adds a Hanzi writer into the element with the ID defined above.
export async function createWriter (userOpts: WriterOpts): Promise<void> {
  const opts = { ...writerOptsDefaults, ...userOpts }
  const container = opts.addTo
  if (!(container instanceof HTMLElement)) {
    throw new Error(`#${container.outerHTML} is not a HTMLElement`)
  }
  const writersContainer = document.createElement('div')
  writersContainer.classList.add('writers')
  const strokesContainer = document.createElement('div')
  container.append(writersContainer, strokesContainer)
  const write = await initWrite(writersContainer, strokesContainer, opts)
  animate(write, opts.animate)
  for (const writer of write.writers) {
    writer.hanziWriter.target.node.addEventListener(
      'click', () => { selectWriter(write, writer) }
    )
  }
}

/// Creates the necessary state and DOM elements for the later animations.
async function initWrite (
  writersContainer: HTMLDivElement,
  strokesContainer: HTMLDivElement,
  opts: WriterOpts): Promise<WriteReady> {
  const writers: Writer[] = []
  for (const char of opts.text) {
    if (isMaybeHanzi(char)) {
      const speed = (typeof opts?.animate?.strokes?.length === 'number')
        ? 0.5
        : 1.75
      const size = (opts?.isLarge === true) ? hanziSizeLarge : hanziSize
      const hanziWriterOpts: Partial<HanziWriterOptions> = {
        width: size,
        height: size,
        padding: 0,
        strokeColor: colorFg,
        outlineColor: colorInactive,
        strokeAnimationSpeed: speed,
        delayBetweenStrokes
      }
      if (opts.highlightRadical === true) {
        hanziWriterOpts.radicalColor = colorHighlight
      }
      hanziWriterOpts.charDataLoader = (_char, onComplete, onError) => {
        getHanziData({ char, kind: opts.kind }).then(
          data => {
            if (data !== undefined) {
              onComplete(data)
            } else {
              const msg = `No character SVG available for ${char}`
              error(msg)
              onError(msg)
            }
          },
          onError
        )
      }
      const hanziWriterContainer = document.createElement('div')
      hanziWriterContainer.classList.add('writer')
      writersContainer.append(hanziWriterContainer)
      const hanziWriter = HanziWriter.create(
        hanziWriterContainer, char, hanziWriterOpts
      )
      hanziWriter.target.node.classList.add('writer-hanzi-writer')
      setActive(hanziWriterContainer, false)
      const strokes = await createStrokes({
        appendTo: strokesContainer,
        character: char,
        kind: opts.kind
      })
      setActive(strokes.container, false)
      writers.push({
        container: hanziWriterContainer,
        character: char,
        hanziWriter,
        strokes
      })
    }
  }
  return {
    state: State.Ready,
    writers,
    selectedWriter: undefined,
    animatedIdx: -1,
    animateNextIdx: 0,
    animateNextIncrement: writers.length <= 1 ? 0 : 1,
    isAnimating: opts.animate?.no !== true,
    continuationRequested: false
  }
}

/**
 * Selects the specified writer, unless it is already selected. If already
 * selected, de-selects it.
 */
function selectWriter (write: WriteReady, selectedWriter: Writer): void {
  if (write.selectedWriter !== selectedWriter) {
    // new selection: animate that one and show strokes too
    write.selectedWriter = selectedWriter
    write.animateNextIncrement = 0
    write.writers
      .forEach(w => {
        const active = w === selectedWriter
        setActive(w.container, active)
        setActive(w.strokes.container, active)
      })
  } else {
    // was already selected, so de-select all
    write.selectedWriter = undefined
    write.animateNextIncrement = write.writers.length <= 1 ? 0 : 1
    write.writers
      .forEach(w => {
        setActive(w.container, false)
        setActive(w.strokes.container, false)
      })
  }
  // either way continue the animation. if selected, this is looped, if
  // deselected, we animate one more time and then continue advancing
  continueAnimationWith(write, selectedWriter)
}

function setActive (element: Element, active: boolean): void {
  if (active) {
    element.classList.remove('is-inactive')
    element.classList.add('is-active')
  } else {
    element.classList.remove('is-active')
    element.classList.add('is-inactive')
  }
}

function animate (write: WriteReady, opts: AnimateOpts): void {
  if (write.writers.length === 0) {
    return
  }
  let action: () => void
  if (opts.no === true) {
    // no-animate means animate only when interacted with
    action = () => {
      const selected = write.selectedWriter
      if (selected !== undefined) {
        if (write.continuationRequested) {
          write.continuationRequested = false
        }
        void selected.hanziWriter.animateCharacter({
          onComplete: () => {
            if (write.continuationRequested) {
              write.continuationRequested = false
              action()
            } else {
              setTimeout(() => { action() }, keepFullCharacterFor)
            }
          }
        })
        write.isAnimating = true
        write.animatedIdx = write.writers.indexOf(selected)
      } else {
        write.animatedIdx = -1
        write.isAnimating = false
        if (write.animatedIdx >= 0) {
          void write.writers[write.animatedIdx].hanziWriter.setCharacter(
            write.writers[write.animatedIdx].character
          )
        }
      }
    }
  } else if (Array.isArray(opts.strokes)) {
    const strokes = opts.strokes
    action = () => {
      const writerIdx = next()
      const strokeIdx = strokes[writerIdx]
      if (typeof strokeIdx === 'undefined' || strokeIdx < 0) {
        // fall back to animating the whole character if nothing defined
        const writer = write.writers[writerIdx].hanziWriter
        void writer.animateCharacter({
          onComplete: () => {
            if (write.continuationRequested) {
              write.continuationRequested = false
              action()
            } else if (
              writerIdx === (write.writers.length - 1) ||
                write.selectedWriter !== undefined
            ) {
              setTimeout(() => { action() }, keepFullCharacterFor)
            } else {
              setTimeout(() => { action() }, delayBetweenStrokes)
            }
          }
        })
      } else {
        const writer = write.writers[writerIdx]
        // void writer.hanziWriter.setCharacter(writer.character)
        void writer.hanziWriter.animateStroke(strokeIdx, {
          onComplete: () => {
            if (write.continuationRequested) {
              write.continuationRequested = false
              action()
            } else if (
              writerIdx === (write.writers.length - 1) ||
                write.selectedWriter !== undefined
            ) {
              setTimeout(() => { action() }, keepFullCharacterFor)
            } else {
              setTimeout(() => { action() }, delayBetweenStrokes)
            }
          }
        })
      }
    }
  } else {
    action = () => {
      const writerIdx = next()
      const writer = write.writers[writerIdx]
      void writer.hanziWriter.animateCharacter({
        onComplete: () => {
          if (write.continuationRequested) {
            write.continuationRequested = false
            action()
          } else if (
            writerIdx === (write.writers.length - 1) ||
              write.selectedWriter !== undefined
          ) {
            setTimeout(() => { action() }, keepFullCharacterFor)
          } else {
            setTimeout(() => { action() }, delayBetweenStrokes)
          }
        }
      })
    }
  }
  action()

  function next (): number {
    write.animatedIdx = write.animateNextIdx
    write.animateNextIdx += write.animateNextIncrement
    if (write.animateNextIdx < 0) {
      write.animateNextIdx = write.writers.length - 1
    } else if (write.animateNextIdx >= write.writers.length) {
      write.animateNextIdx = 0
    }
    return write.animatedIdx
  }
}

/**
 * Resets the currently running animation and continues with the specified
 * Hanzi writer.
 */
function continueAnimationWith (
  write: WriteReady,
  continueWith: Writer
): void {
  const { writers, animatedIdx } = write
  write.continuationRequested = true
  if (!write.isAnimating) {
    // when not already animating, start now
    animate(write, { no: true })
  } else if (animatedIdx >= 0) {
    const currentWriter = writers[animatedIdx]
    write.animateNextIdx = writers.indexOf(continueWith)
    // reset all current animations, which also triggers the callback
    // to advance the animation to the newly set next index
    void currentWriter.hanziWriter.setCharacter(currentWriter.character)
  }
}
