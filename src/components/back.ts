import { error } from './debug/log'
import { init as initComponents } from './init'
// init both front and back components
void initComponents(document.body).then(
  undefined,
  err => { error(JSON.stringify(err)) }
)
