import { readFile } from 'fs/promises'

export interface GetDataOpts {
  /**
   * The JS file to load for the data, e.g. `hanzi-data/_hdA.js'.
   *
   * Directories are handled in testing, but only the basename is considered in
   * Anki, which does not support directories.
   */
  path: string
  /**
   * First argument to the JSONP callback before the actual data.
   */
  key: string
}

const jsonpCallbackFnName = 'hd'
/** keys against functions to call on resolve. */
const jsonpWaiters = new Map<string, Array<(unknown) => void>>()
const cached: Record<string, Readonly<unknown>> = {}
const canAccessFilesystem = typeof document === 'undefined'

/**
 * Fetches JSONP data with the specified key.
 *
 * For a key of A, will fetch `_hdA.js`, assuming it contains a function call
 * to `hd`, with the key as the first param and the data (unknown type) as the
 * second.
 *
 * Unsafe because it is up to the caller to cast to a concrete correct type.
 */
export async function getDataUnsafe (
  opts: GetDataOpts
): Promise<Readonly<unknown>> {
  const { key } = opts
  let data: Readonly<unknown>
  if (key in cached) {
    data = cached[key]
  } else if (canAccessFilesystem) {
    // read directly from disk when running in test and skip the cache
    data = await fetchNonBrowser(opts)
  } else {
    // in production and parcel use a JSONP-like API because everything else
    // failed in AnkiDroid
    data = await fetchJsonp(opts)
    // caching helps a lot with load speed since the same character often
    // occurs multiple times on the same card
    cached[key] = data
  }
  return data
}

/**
 * Fetches the JSONP via direct filesystem access (for tests).
 */
async function fetchNonBrowser (opts: GetDataOpts): Promise<Readonly<unknown>> {
  if (!('BUILD_PREFIX' in process.env)) {
    throw new Error('Dunno where the data at, what BUILD_PREFIX?')
  }
  const path = `${process.env.BUILD_PREFIX}${opts.path}`
  const jsonp = await readFile(path, { encoding: 'utf-8' })
  const data = JSON.parse(jsonp.substring(
    `${jsonpCallbackFnName}(${JSON.stringify(opts.key)},`.length,
    jsonp.length - ')'.length
  ))
  // just for testing: we want to catch accidental modifications of data that
  //                   would be shared if this were production
  Object.freeze(data)
  return data
}

/**
 * Callback for the JSONP scripts with the data.
 *
 * Raw JSON would not work with AnkiDroid, which does not support XHR/fetch,
 * but allows scripts.
 *
 * In AnkiDroid, every card gets a fresh window, but desktop and Anki Web
 * preserved the callback from the last call. If window already contains a
 * callback, it works to just overwrite the old callback here that is no longer
 * needed. See #298 for details.
 */
if (!canAccessFilesystem) {
  window[jsonpCallbackFnName] = (char: string, data: Readonly<unknown>) => {
    (jsonpWaiters.get(char) ?? []).forEach(w => {
      // careful: every consumer gets an identical object, don't modify
      w(data)
    })
    jsonpWaiters.delete(char)
  }
}

/**
 * Registers a waiter for the specified key.
 *
 * It will be removed automatically after the data has been received.
 */
function registerJsonpWaiter (
  char: string,
  callback: (data: Readonly<unknown>) => void
): void {
  jsonpWaiters.set(
    char,
    [...(jsonpWaiters.get(char) ?? []), callback]
  )
}

/**
 * Fetches data over the network for the specified key.
 */
async function fetchJsonp (opts: GetDataOpts): Promise<Readonly<unknown>> {
  return await new Promise((resolve, reject) => {
    const url = process.env.NODE_ENV === 'production'
      // No directories for Anki media possible, for prod use just filename
      ? opts.path.split('/').pop()!
      // For testing in parcel it works better to have a subpath that we can
      // exclude from parcel.
      : `/${process.env.BUILD_PREFIX}${opts.path}`
    const existingScript = document.querySelector(`script[src="${url}"]`)
    if (existingScript !== null) {
      // already being fetched, append to the list of existing waiters
      registerJsonpWaiter(opts.key, resolve)
      existingScript.addEventListener('error', () => {
        reject(new Error(`No character data available for ${opts.key}`))
      })
    } else {
      // no fetch in flight, inject a new script tag
      const script = document.createElement('script')
      registerJsonpWaiter(
        opts.key,
        data => {
          script.remove()
          resolve(data)
        }
      )
      script.async = true
      script.src = url
      script.onerror = () => {
        script.remove()
        reject(new Error(`No character data available for ${opts.key}`))
      }
      document.body.appendChild(script)
    }
  })
}
