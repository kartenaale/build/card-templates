export function isQuestion (): boolean {
  return !isAnswer()
}

export function isAnswer (): boolean {
  return document.querySelector('#answer') !== null
}
