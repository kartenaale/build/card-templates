import type { HanziData } from '../../../src-common/hanzi-data'
import { getDataUnsafe } from '../data'
import { lookup } from './lut'

export interface GetHanziDataOpts {
  char: string
  kind?: GetHanziDataKind
}

export const enum GetHanziDataKind {
  ANY = 'any',
  DEFAULT = 'default',
  TRADITIONAL = 'traditional'
}

export async function getHanziProp (
  { prop, ...opts }: GetHanziDataOpts & { prop: string }
): Promise<string | string[] | undefined> {
  const data = await getHanziData(opts)
  if (data === undefined) {
    return undefined
  }
  return data[prop] ?? lookup(data, prop)
}

export async function getHanziData (
  { char, kind }: GetHanziDataOpts
): Promise<Readonly<HanziData>> {
  if (char.length !== 1) {
    throw new Error(`Can only get data for single chars, got: ${char}`)
  }

  const data = (await getDataUnsafe({ path: `hanzi-data/_cd${char}.js`, key: char })) as Readonly<HanziData>
  if (kind === GetHanziDataKind.TRADITIONAL && data.trad !== undefined) {
    // data in the cache is read-only => make a fresh copy with trad overrides
    return {
      ...data,
      ...data.trad
    }
  }
  return data
}
