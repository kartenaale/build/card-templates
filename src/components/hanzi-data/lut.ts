import { StrokeType } from '../../../src-common/stroke-encodings'
import { type HanziData } from '../../../src-common/hanzi-data'

type Lut<T> = Record<string, T>

interface RadicalMeanings {
  radicalMeaningZh: string
  radicalMeaningDe: string
}

const radicalMeanings: Lut<RadicalMeanings> = {}
const strokeTypeNumbers: Record<StrokeType, string> = {
  [StrokeType.HENG]: '①',
  [StrokeType.TI]: '①',
  [StrokeType.SHU]: '②',
  [StrokeType.SHUGOU]: '②',
  [StrokeType.PIE]: '③',
  [StrokeType.DIAN]: '④',
  [StrokeType.NA]: '④',
  [StrokeType.WANGOU]: '⑤',
  [StrokeType.XIEGOU]: '⑤',
  [StrokeType.WOGOU]: '⑤',
  [StrokeType.SHUWAN]: '⑤',
  [StrokeType.HENGZHEZHE]: '⑤',
  [StrokeType.HENGZHEGOU]: '⑤',
  [StrokeType.HENGPIE]: '⑤',
  [StrokeType.HENGZHEWANGOU]: '⑤',
  [StrokeType.SHUZHEZHEGOU]: '⑤',
  [StrokeType.HENGZHETI]: '⑤',
  [StrokeType.HENGZHEZHEPIE]: '⑤',
  [StrokeType.HENGPIEWANGOU]: '⑤',
  [StrokeType.HENGZHEWAN]: '⑤',
  [StrokeType.HENGZHEZHEZHE]: '⑤',
  [StrokeType.HENGZHE]: '⑤',
  [StrokeType.HENGGOU]: '⑤',
  [StrokeType.SHUZHE]: '⑤',
  [StrokeType.SHUTI]: '⑤',
  [StrokeType.PIEDIAN]: '⑤',
  [StrokeType.PIEZHE]: '⑤',
  [StrokeType.SHUZHEZHE]: '⑤',
  [StrokeType.SHUWANGOU]: '⑤',
  [StrokeType.HENGXIEGOU]: '⑤',
  [StrokeType.HENGZHEZHEZHEGOU]: '⑤',
  [StrokeType.SHUZHEPIE]: '⑤'
}
const strokeTypeNames: Record<StrokeType, string> = {
  [StrokeType.HENG]: '横',
  [StrokeType.SHU]: '竖',
  [StrokeType.PIE]: '撇',
  [StrokeType.DIAN]: '点',
  [StrokeType.NA]: '捺',
  [StrokeType.TI]: '提',
  [StrokeType.HENGGOU]: '横钩',
  [StrokeType.HENGZHE]: '横折',
  [StrokeType.HENGZHEGOU]: '横折钩',
  [StrokeType.HENGPIE]: '横撇',
  [StrokeType.SHUGOU]: '竖钩',
  [StrokeType.SHUTI]: '竖提',
  [StrokeType.SHUWANGOU]: '竖弯钩',
  [StrokeType.SHUZHEZHEGOU]: '竖折折钩',
  [StrokeType.PIEDIAN]: '撇点',
  [StrokeType.XIEGOU]: '斜钩',
  [StrokeType.HENGZHETI]: '横折提',
  [StrokeType.HENGZHEWAN]: '横折弯',
  [StrokeType.HENGZHEZHE]: '横折折',
  [StrokeType.HENGXIEGOU]: '横斜钩',
  [StrokeType.HENGZHEWANGOU]: '横折弯钩',
  [StrokeType.HENGPIEWANGOU]: '横撇弯钩',
  [StrokeType.HENGZHEZHEPIE]: '横折折撇',
  [StrokeType.HENGZHEZHEZHEGOU]: '横折折折钩',
  [StrokeType.HENGZHEZHEZHE]: '横折折折',
  [StrokeType.SHUZHE]: '竖折',
  [StrokeType.SHUWAN]: '竖弯',
  [StrokeType.SHUZHEPIE]: '竖折撇',
  [StrokeType.SHUZHEZHE]: '竖折折',
  [StrokeType.PIEZHE]: '撇折',
  [StrokeType.WANGOU]: '弯钩',
  [StrokeType.WOGOU]: '卧钩'
}

export type PropId = keyof RadicalMeanings |
'strokeTypeNames' |
'strokeTypeNumbers'

export function lookup (
  item: HanziData,
  propId: PropId | string
): string | string[] | undefined {
  if (propId === 'radicalMeaningDe' || propId === 'radicalMeaningZh') {
    if (item.radical in radicalMeanings) {
      return radicalMeanings[item.radical][propId]
    }
  } else if (propId === 'strokeTypeNames') {
    return item.strokeTypes.map(t => strokeTypeNames[t])
  } else if (propId === 'strokeTypeNumbers') {
    return item.strokeTypes.map(t => strokeTypeNumbers[t])
  }
  return undefined
}

function defineRadical (
  radicalVariants: string[],
  meanings: RadicalMeanings): void {
  for (const radical of radicalVariants) {
    if (radical in radicalMeanings) {
      throw new Error(`${radical} defined more than once`)
    }
    radicalMeanings[radical] = meanings
  }
}

// https://de.wikipedia.org/wiki/227_Radikale
defineRadical(
  ['丶'],
  { radicalMeaningDe: 'Punkt', radicalMeaningZh: 'diǎn, zhǔ' }
)
defineRadical(
  ['一'],
  { radicalMeaningDe: 'eins', radicalMeaningZh: 'héng, yī' }
)
defineRadical(
  ['丨'],
  { radicalMeaningDe: 'Senkrechtstrich', radicalMeaningZh: 'shù' }
)
defineRadical(
  ['丿', '乀', '乁'],
  { radicalMeaningDe: 'Schrägstrich', radicalMeaningZh: 'piě' }
)
defineRadical(
  ['乛'],
  { radicalMeaningDe: '', radicalMeaningZh: 'hénggōu' }
)
defineRadical(
  ['𠃌'],
  { radicalMeaningDe: '', radicalMeaningZh: 'héngpiě' }
)
defineRadical(
  ['乙', '乚'],
  { radicalMeaningDe: '2. Himmelsstamm', radicalMeaningZh: 'yǐ' }
)
defineRadical(
  ['冫'],
  {
    radicalMeaningDe: 'Eis, (Zwei-Punkte-Wasser)',
    radicalMeaningZh: 'liǎngdiǎn shuǐ 两点水'
  }
)
defineRadical(
  ['亠'],
  { radicalMeaningDe: 'Deckel', radicalMeaningZh: 'tóu' }
)
defineRadical(
  ['讠'],
  { radicalMeaningDe: 'sprechen', radicalMeaningZh: 'yán 言字旁' }
)
defineRadical(
  ['二'],
  { radicalMeaningDe: 'zwei', radicalMeaningZh: 'èr' }
)
defineRadical(
  ['十'],
  { radicalMeaningDe: 'zehn', radicalMeaningZh: 'shí 十字儿' }
)
defineRadical(
  ['厂'],
  { radicalMeaningDe: 'Abhang, Fabrik', radicalMeaningZh: 'cháng 厂字旁' }
)
defineRadical(
  ['𠂇'],
  { radicalMeaningDe: '', radicalMeaningZh: 'zuǒ' }
)
defineRadical(
  ['匚'],
  { radicalMeaningDe: 'Rahmen', radicalMeaningZh: 'fāng 三框儿' }
)
defineRadical(
  ['卜'],
  { radicalMeaningDe: 'Orakel, wahrsagen', radicalMeaningZh: 'bǔ' }
)
defineRadical(
  ['刂'],
  { radicalMeaningDe: 'Messer', radicalMeaningZh: 'dāo 立刀旁儿' }
)
defineRadical(
  ['冖'],
  { radicalMeaningDe: 'Deckel, Krone', radicalMeaningZh: 'mì 秃宝盖儿' }
)
defineRadical(
  ['冂'],
  { radicalMeaningDe: 'Begrenzung', radicalMeaningZh: 'jiōng 同字匡儿' }
)
defineRadical(
  ['𠂉'],
  { radicalMeaningDe: '', radicalMeaningZh: '' }
)
defineRadical(
  ['亻'],
  {
    radicalMeaningDe: 'Mensch (einzelner Mensch)',
    radicalMeaningZh: 'dānrén páng 单人旁儿'
  }
)
defineRadical(
  ['⺁'],
  { radicalMeaningDe: '', radicalMeaningZh: '' }
)
defineRadical(
  ['人'],
  { radicalMeaningDe: 'Mensch', radicalMeaningZh: 'rén' }
)
defineRadical(
  ['八', '丷'],
  { radicalMeaningDe: 'acht', radicalMeaningZh: 'bā' }
)
defineRadical(
  ['乂'],
  { radicalMeaningDe: '', radicalMeaningZh: '' }
)
defineRadical(
  ['勹'],
  {
    radicalMeaningDe: 'einpacken, (Rahmen des Zeichens bao)',
    radicalMeaningZh: 'bāozi tóu 包字头儿'
  }
)
defineRadical(
  ['刀'],
  { radicalMeaningDe: 'Messer, Schwert', radicalMeaningZh: 'dāo' }
)
defineRadical(
  ['力'],
  { radicalMeaningDe: 'Kraft', radicalMeaningZh: 'lì' }
)
defineRadical(
  ['儿'],
  { radicalMeaningDe: 'Beine, Kind', radicalMeaningZh: 'ér' }
)
defineRadical(
  ['几'],
  { radicalMeaningDe: 'Tischchen', radicalMeaningZh: 'jī' }
)
defineRadical(
  ['龴'],
  { radicalMeaningDe: '', radicalMeaningZh: '' }
)
defineRadical(
  ['㔾', '卩'],
  { radicalMeaningDe: 'Siegel', radicalMeaningZh: 'jié' }
)
defineRadical(
  ['阝'],
  {
    radicalMeaningDe: 'links: Hügel, linkes Ohr bzw. rechts: Stadt, rechtes Ohr',
    radicalMeaningZh: 'zuǒ ěrduo 单耳旁儿 / yòu ěrduo 双耳旁儿'
  }
)
defineRadical(
  ['阜', '⻖'],
  {
    radicalMeaningDe: 'Hügel, (linkes Ohr, immer links)',
    radicalMeaningZh: 'zuǒ ěrduo 单耳旁儿'
  }
)
defineRadical(
  ['邑', '⻏'],
  {
    radicalMeaningDe: 'Dorf, Stadt, (rechtes Ohr, immer rechts)',
    radicalMeaningZh: 'yòu ěrduo 双耳旁儿'
  }
)
defineRadical(
  ['又'],
  { radicalMeaningDe: 'rechte Hand; auch, wieder', radicalMeaningZh: 'yòu' }
)
defineRadical(
  ['廴'],
  {
    radicalMeaningDe: 'marschieren, (Radikal des Zeichens jian)',
    radicalMeaningZh: 'yín; 建之旁儿 jiàn zì páng'
  }
)
defineRadical(
  ['厶'],
  {
    radicalMeaningDe: 'Kokon, privat, persönlich',
    radicalMeaningZh: 'sī 私字儿'
  }
)
defineRadical(
  ['凵'],
  { radicalMeaningDe: 'Schüssel, offener Mund', radicalMeaningZh: 'qǔ' }
)
defineRadical(
  ['匕'],
  { radicalMeaningDe: 'Löffel', radicalMeaningZh: 'bǐ' }
)
defineRadical(
  ['氵'],
  {
    radicalMeaningDe: 'Wasser (3-Punkte-Wasser)',
    radicalMeaningZh: '三点水儿 sāndiǎn shuǐ'
  }
)
defineRadical(
  ['忄', '㣺'],
  {
    radicalMeaningDe: 'Herz',
    radicalMeaningZh: 'shù xīn páng 竖心旁儿 = senkrechtes Herz'
  }
)
defineRadical(
  ['丬', '爿'],
  {
    radicalMeaningDe: 'gespaltener Bambus, (Radikal des Zeichens jiang)',
    radicalMeaningZh: 'pán, 将字旁儿 jiàng zì páng'
  }
)
defineRadical(
  ['亡'],
  { radicalMeaningDe: 'sterben, fliehen, flüchten', radicalMeaningZh: 'wáng' }
)
defineRadical(
  ['广'],
  {
    radicalMeaningDe: 'weit, breit, ausgedehnt',
    radicalMeaningZh: 'guǎng 广字旁儿'
  }
)
defineRadical(
  ['宀'],
  {
    radicalMeaningDe: 'Dach',
    radicalMeaningZh: 'mián'
  }
)
defineRadical(
  ['门', '門'],
  { radicalMeaningDe: 'Tür, Tor', radicalMeaningZh: 'mén' }
)
defineRadical(
  ['辶', '辵'],
  {
    radicalMeaningDe: 'anhalten, (Radikal des Zeichens zou)',
    radicalMeaningZh: 'chuò, 走之儿 zǒu zì páng'
  }
)
defineRadical(
  ['工'],
  { radicalMeaningDe: 'Arbeit, arbeiten', radicalMeaningZh: 'gōng' }
)
defineRadical(
  ['土'],
  { radicalMeaningDe: 'Erde, Boden', radicalMeaningZh: 'tǔ' }
)
defineRadical(
  ['艹', '艸'],
  {
    radicalMeaningDe: 'Gras',
    radicalMeaningZh: 'cǎo zì tóu 草字头儿'
  }
)
defineRadical(
  ['廾'],
  {
    radicalMeaningDe: 'erhobene Hände, (unterer Teil des Zeichens gong)',
    radicalMeaningZh: 'nòng zì dǐr 弄字底儿'
  }
)
defineRadical(
  ['大'],
  { radicalMeaningDe: 'groß', radicalMeaningZh: 'dà' }
)
defineRadical(
  ['尣', '尢'],
  { radicalMeaningDe: 'lahm', radicalMeaningZh: 'yóu, wāng 尤字旁儿' }
)
defineRadical(
  ['寸'],
  { radicalMeaningDe: 'Daumen, Zoll', radicalMeaningZh: 'cùn' }
)
defineRadical(
  ['扌', '龵'],
  { radicalMeaningDe: 'Hand, (Radikal Hand)', radicalMeaningZh: 'tíshǒu páng' }
)
defineRadical(
  ['弋'],
  { radicalMeaningDe: 'Wurfspieß, Pfeil mit Schnur', radicalMeaningZh: 'yì' }
)
defineRadical(
  ['巾'],
  { radicalMeaningDe: 'Tuch, Schal', radicalMeaningZh: 'jīn' }
)
defineRadical(
  ['口'],
  { radicalMeaningDe: 'Mund, Öffnung', radicalMeaningZh: 'kǒu' }
)
defineRadical(
  ['囗'],
  {
    radicalMeaningDe: 'umgeben, (Rahmen des Zeichens wei)',
    radicalMeaningZh: 'wéi, wéi zì kuāng'
  }
)
defineRadical(
  ['山'],
  { radicalMeaningDe: 'Berg, Gebirge', radicalMeaningZh: 'shān' }
)
defineRadical(
  ['屮'],
  { radicalMeaningDe: 'Spross, Trieb, junges Gras', radicalMeaningZh: 'chè' }
)
defineRadical(
  ['彳'],
  {
    radicalMeaningDe: 'Schritt, schlendern, bummeln',
    radicalMeaningZh: 'chì 双人旁儿'
  }
)
defineRadical(
  ['彡'],
  {
    radicalMeaningDe: 'Haarsträne, (drei gebogene Striche)',
    radicalMeaningZh: 'sān piě 三撇儿'
  }
)
defineRadical(
  ['夕'],
  {
    radicalMeaningDe: 'Dämmerung, Sonnenuntergang',
    radicalMeaningZh: 'xī'
  }
)
defineRadical(
  ['夂'],
  {
    radicalMeaningDe: 'verfolgen, langsam, (Kopf des Zeichens tou)',
    radicalMeaningZh: 'zhǐ, dōng zì tóu 冬字头, 折文儿'
  }
)
defineRadical(
  ['丸'],
  {
    radicalMeaningDe: 'Kugel, Ball, Pille',
    radicalMeaningZh: 'wán'
  }
)
defineRadical(
  ['尸'],
  { radicalMeaningDe: 'Leichnam', radicalMeaningZh: 'shī' }
)
defineRadical(
  ['饣'],
  {
    radicalMeaningDe: 'Nahrung, (Radikal des Zeichens shi)',
    radicalMeaningZh: '食字旁儿 shí zì páng'
  }
)
defineRadical(
  ['犭'],
  {
    radicalMeaningDe: 'Hund, (umgedrehter Hund)',
    radicalMeaningZh: 'fan quan pang 反犬旁儿'
  }
)
defineRadical(
  ['彐', '彑'],
  { radicalMeaningDe: 'Schweinerüssel', radicalMeaningZh: 'jì' }
)
defineRadical(
  ['弓'],
  { radicalMeaningDe: 'Bogen', radicalMeaningZh: 'gōng' }
)
defineRadical(
  ['己'],
  { radicalMeaningDe: 'selbst, persönlich, bereits', radicalMeaningZh: 'Jǐ' }
)
defineRadical(
  ['女'],
  { radicalMeaningDe: 'Frau', radicalMeaningZh: 'nǚ' }
)
defineRadical(
  ['子'],
  { radicalMeaningDe: 'Kind', radicalMeaningZh: 'zǐ' }
)
defineRadical(
  ['马', '馬'],
  { radicalMeaningDe: 'Pferd', radicalMeaningZh: 'mǎ' }
)
defineRadical(
  ['幺'],
  { radicalMeaningDe: 'eins', radicalMeaningZh: 'yāo' }
)
defineRadical(
  ['纟', '糹', '糸'],
  { radicalMeaningDe: 'Seide', radicalMeaningZh: 'mì (sī) 绞丝旁儿' }
)
defineRadical(
  ['巛', '巜', '川'],
  {
    radicalMeaningDe: 'Strom, Fluss, drei Winkelstriche',
    radicalMeaningZh: 'chuān, sān guài'
  }
)
defineRadical(
  ['小'],
  { radicalMeaningDe: 'klein', radicalMeaningZh: 'Xiǎo' }
)
defineRadical(
  ['灬'],
  { radicalMeaningDe: 'Feuer', radicalMeaningZh: 'biāo, huǒ 四点儿' }
)
defineRadical(
  ['心'],
  { radicalMeaningDe: 'Herz', radicalMeaningZh: 'xīn' }
)
defineRadical(
  ['斗'],
  { radicalMeaningDe: 'Scheffel, Hohlmaß', radicalMeaningZh: 'dòu' }
)
defineRadical(
  ['火'],
  { radicalMeaningDe: 'Feuer, Flamme', radicalMeaningZh: 'huǒ 火字旁儿' }
)
defineRadical(
  ['文'],
  {
    radicalMeaningDe: 'Muster, Kultur, Schrift, Text',
    radicalMeaningZh: 'wén'
  }
)
defineRadical(
  ['方'],
  { radicalMeaningDe: 'Viereck, Himmelsrichtung', radicalMeaningZh: 'fāng' }
)
defineRadical(
  ['户', '戸', '戶'],
  { radicalMeaningDe: 'Türflügel', radicalMeaningZh: 'hù 户字旁儿' }
)
defineRadical(
  ['礻'],
  {
    radicalMeaningDe: 'Hinweis, (Radikal des Zeichens shi)',
    radicalMeaningZh: 'shì zì páng 示字旁儿'
  }
)
defineRadical(
  ['王', '⺩'],
  { radicalMeaningDe: 'König', radicalMeaningZh: 'wáng 王字旁儿' }
)
defineRadical(
  ['主'],
  { radicalMeaningDe: 'Herrscher, Gastgeber', radicalMeaningZh: 'zhǔ' }
)
defineRadical(
  ['天'],
  { radicalMeaningDe: 'Himmel, Tag', radicalMeaningZh: 'tiān' }
)
defineRadical(
  ['韦', '韋'],
  { radicalMeaningDe: 'Leder', radicalMeaningZh: 'wéi' }
)
defineRadical(
  ['耂', '老'],
  { radicalMeaningDe: 'alt', radicalMeaningZh: 'lǎo' }
)
defineRadical(
  ['廿'],
  { radicalMeaningDe: 'zwanzig', radicalMeaningZh: 'niàn' }
)
defineRadical(
  ['木'],
  { radicalMeaningDe: 'Baum, Holz', radicalMeaningZh: 'mù' }
)
defineRadical(
  ['不'],
  { radicalMeaningDe: 'nicht', radicalMeaningZh: 'bù' }
)
defineRadical(
  ['犬'],
  { radicalMeaningDe: 'Hund', radicalMeaningZh: 'quǎn' }
)
defineRadical(
  ['歹', '歺'],
  { radicalMeaningDe: 'schlecht, böse', radicalMeaningZh: 'dǎi' }
)
defineRadical(
  ['瓦'],
  { radicalMeaningDe: 'Ziegel', radicalMeaningZh: 'wǎ' }
)
defineRadical(
  ['牙'],
  { radicalMeaningDe: 'Eckzahn', radicalMeaningZh: 'yá' }
)
defineRadical(
  ['车', '車'],
  { radicalMeaningDe: 'Wagen, Fahrzeug', radicalMeaningZh: 'chē' }
)
defineRadical(
  ['戈'],
  { radicalMeaningDe: 'Lanze, Hiebaxt', radicalMeaningZh: 'gē' }
)
defineRadical(
  ['止'],
  {
    radicalMeaningDe: 'Zehe, stehen bleiben, unterbrechen',
    radicalMeaningZh: 'zhǐ'
  }
)
defineRadical(
  ['日'],
  { radicalMeaningDe: 'Sonne', radicalMeaningZh: 'rì' }
)
defineRadical(
  ['曰'],
  { radicalMeaningDe: 'sprechen, sagen', radicalMeaningZh: 'yuē' }
)
defineRadical(
  ['中'],
  { radicalMeaningDe: 'Mitte', radicalMeaningZh: 'zhōng' }
)
defineRadical(
  ['贝', '貝'],
  { radicalMeaningDe: 'Kaurimuschel', radicalMeaningZh: 'bèi' }
)
defineRadical(
  ['见', '見'],
  { radicalMeaningDe: 'sehen', radicalMeaningZh: 'jiàn' }
)
defineRadical(
  ['父'],
  { radicalMeaningDe: 'Vater', radicalMeaningZh: 'fù' }
)
defineRadical(
  ['气'],
  { radicalMeaningDe: 'Dampf, Gas, Luft', radicalMeaningZh: 'qì' }
)
defineRadical(
  ['牜', '牛'],
  { radicalMeaningDe: 'Rind', radicalMeaningZh: 'niú' }
)
defineRadical(
  ['手'],
  { radicalMeaningDe: 'Hand', radicalMeaningZh: 'shǒu' }
)
defineRadical(
  ['毛'],
  { radicalMeaningDe: 'Haar, Borste', radicalMeaningZh: 'máo' }
)
defineRadical(
  ['攵', '攴'],
  { radicalMeaningDe: 'klopfen', radicalMeaningZh: 'pū' }
)
defineRadical(
  ['片'],
  { radicalMeaningDe: 'Scheibe, Platte', radicalMeaningZh: 'piàn' }
)
defineRadical(
  ['斤'],
  { radicalMeaningDe: 'Axt, Pfund', radicalMeaningZh: 'jīn' }
)
defineRadical(
  ['爫', '爪'],
  { radicalMeaningDe: 'Klaue', radicalMeaningZh: 'zhuā' }
)
defineRadical(
  ['尺'],
  { radicalMeaningDe: 'Fuß (Längenmaß)', radicalMeaningZh: 'chǐ' }
)
defineRadical(
  ['月', '肉'],
  { radicalMeaningDe: 'Mond, Monat, Fleisch', radicalMeaningZh: 'yuè' }
)
defineRadical(
  ['殳'],
  { radicalMeaningDe: 'Keule, Lanze aus Bambus', radicalMeaningZh: 'shū' }
)
defineRadical(
  ['欠'],
  { radicalMeaningDe: 'gähnen, schulden, mangeln', radicalMeaningZh: 'qiàn' }
)
defineRadical(
  ['风', '風'],
  { radicalMeaningDe: 'Wind', radicalMeaningZh: 'fēng' }
)
defineRadical(
  ['氏'],
  { radicalMeaningDe: 'Sippe, Familie', radicalMeaningZh: 'shì' }
)
defineRadical(
  ['比'],
  { radicalMeaningDe: 'vergleichen', radicalMeaningZh: 'bǐ' }
)
defineRadical(
  ['肀', '聿'],
  {
    radicalMeaningDe: 'Pinsel, (Kopf des Zeichens yu)',
    radicalMeaningZh: 'yù zì tóu'
  }
)
defineRadical(
  ['水', '氺'],
  { radicalMeaningDe: 'Wasser', radicalMeaningZh: 'shuǐ' }
)
defineRadical(
  ['立'],
  { radicalMeaningDe: 'stehen, aufrichten', radicalMeaningZh: 'lì' }
)
defineRadical(
  ['疒'],
  {
    radicalMeaningDe: 'Krankheit, (Radikal des Zeichens bing)',
    radicalMeaningZh: 'nè'
  }
)
defineRadical(
  ['穴'],
  { radicalMeaningDe: 'Höhle', radicalMeaningZh: 'xuè' }
)
defineRadical(
  ['衤'],
  { radicalMeaningDe: 'Kleidung', radicalMeaningZh: 'yī' }
)
defineRadical(
  ['𡗗'],
  { radicalMeaningDe: '', radicalMeaningZh: '' }
)
defineRadical(
  ['玉'],
  { radicalMeaningDe: 'Jade', radicalMeaningZh: 'yù' }
)
defineRadical(
  ['示'],
  { radicalMeaningDe: 'Hinweis, Zeichen', radicalMeaningZh: 'shì' }
)
defineRadical(
  ['去'],
  { radicalMeaningDe: 'weg gehen', radicalMeaningZh: 'qù' }
)
defineRadical(
  ['𤇾'],
  {
    radicalMeaningDe: '',
    radicalMeaningZh: 'róng zì tóu 荣字头'
  }
)
defineRadical(
  ['甘'],
  { radicalMeaningDe: 'süß', radicalMeaningZh: 'gān' }
)
defineRadical(
  ['石'],
  { radicalMeaningDe: 'Stein', radicalMeaningZh: 'shí' }
)
defineRadical(
  ['龙', '龍'],
  { radicalMeaningDe: 'Drachen', radicalMeaningZh: 'lóng' }
)
defineRadical(
  ['戊'],
  { radicalMeaningDe: '5. Himmelstamm', radicalMeaningZh: 'wù' }
)
defineRadical(
  ['龸'],
  {
    radicalMeaningDe: '',
    radicalMeaningZh: 'cháng zì tóu 常字頭'
  }
)
defineRadical(
  ['业'],
  { radicalMeaningDe: 'Geschäft, Branche', radicalMeaningZh: 'yè' }
)
defineRadical(
  ['目'],
  { radicalMeaningDe: 'Auge', radicalMeaningZh: 'mù' }
)
defineRadical(
  ['田'],
  { radicalMeaningDe: 'Feld', radicalMeaningZh: 'tián' }
)
defineRadical(
  ['由'],
  { radicalMeaningDe: 'Grund, Ursache, wegen, durch', radicalMeaningZh: 'yóu' }
)
defineRadical(
  ['申'],
  { radicalMeaningDe: 'erklären, 9. Erdzweig', radicalMeaningZh: 'shēn' }
)
defineRadical(
  ['罒'],
  { radicalMeaningDe: 'Netz', radicalMeaningZh: 'wǎng 皿字头' }
)
defineRadical(
  ['皿'],
  { radicalMeaningDe: 'Schüssel, Geschirr', radicalMeaningZh: 'mǐn 皿字底' }
)
defineRadical(
  ['钅'],
  { radicalMeaningDe: 'Gold, Metall', radicalMeaningZh: 'jīn' }
)
defineRadical(
  ['矢'],
  { radicalMeaningDe: 'Pfeil', radicalMeaningZh: 'shǐ' }
)
defineRadical(
  ['禾'],
  { radicalMeaningDe: 'Getreide', radicalMeaningZh: 'hé' }
)
defineRadical(
  ['白'],
  { radicalMeaningDe: 'weiß', radicalMeaningZh: 'bái' }
)
defineRadical(
  ['瓜'],
  { radicalMeaningDe: 'Melone', radicalMeaningZh: 'guā' }
)
defineRadical(
  ['鸟', '鳥'],
  { radicalMeaningDe: 'Vogel', radicalMeaningZh: 'niǎo' }
)
defineRadical(
  ['皮'],
  { radicalMeaningDe: 'Haut, Leder', radicalMeaningZh: 'pí' }
)
defineRadical(
  ['癶'],
  { radicalMeaningDe: 'Rücken', radicalMeaningZh: 'bō' }
)
defineRadical(
  ['矛'],
  { radicalMeaningDe: 'Speer, Lanze', radicalMeaningZh: 'máo' }
)
defineRadical(
  ['⺪', '疋'],
  { radicalMeaningDe: 'Stoffballen', radicalMeaningZh: 'shū, pǐ' }
)
defineRadical(
  ['羊', '⺶'],
  { radicalMeaningDe: 'Ziege, Schaf', radicalMeaningZh: 'yáng' }
)
defineRadical(
  ['龹'],
  { radicalMeaningDe: 'Rolle', radicalMeaningZh: 'Quán zì tóu 拳字头' }
)
defineRadical(
  ['米'],
  { radicalMeaningDe: 'Reis', radicalMeaningZh: 'mǐ' }
)
defineRadical(
  ['齐', '齊'],
  {
    radicalMeaningDe: 'aufreihen, ordentlich, gleichmäßig',
    radicalMeaningZh: 'qí'
  }
)
defineRadical(
  ['衣'],
  { radicalMeaningDe: 'Kleidung, Kleid', radicalMeaningZh: 'yī' }
)
defineRadical(
  ['亦'],
  { radicalMeaningDe: 'auch, ebenso', radicalMeaningZh: 'yì' }
)
defineRadical(
  ['耳'],
  { radicalMeaningDe: 'Ohr', radicalMeaningZh: 'ěr' }
)
defineRadical(
  ['臣'],
  { radicalMeaningDe: 'Diener; Minister', radicalMeaningZh: 'chén' }
)
defineRadical(
  ['𢦏'],
  { radicalMeaningDe: '', radicalMeaningZh: '' }
)
defineRadical(
  ['西', '襾', '覀'],
  { radicalMeaningDe: 'bedecken, Westen', radicalMeaningZh: 'xī' }
)
defineRadical(
  ['朿'],
  { radicalMeaningDe: 'Dorn', radicalMeaningZh: 'cì' }
)
defineRadical(
  ['亚'],
  {
    radicalMeaningDe: 'unterlegen, zweitrangig, Asien',
    radicalMeaningZh: 'yà'
  }
)
defineRadical(
  ['而'],
  { radicalMeaningDe: 'Bart, und', radicalMeaningZh: 'ér' }
)
defineRadical(
  ['页', '頁'],
  { radicalMeaningDe: 'Kopf, Seite', radicalMeaningZh: 'yè' }
)
defineRadical(
  ['至'],
  { radicalMeaningDe: 'erreichen, bis, nach', radicalMeaningZh: 'zhì' }
)
defineRadical(
  ['光'],
  { radicalMeaningDe: 'Glanz, Licht, Strahl', radicalMeaningZh: 'guāng' }
)
defineRadical(
  ['虍'],
  {
    radicalMeaningDe: 'Tiger, (Kopf des Zeichens Tiger)',
    radicalMeaningZh: 'hǔ, hǔ zì tóu'
  }
)
defineRadical(
  ['虫'],
  { radicalMeaningDe: 'Insekt, Wurm', radicalMeaningZh: 'chóng' }
)
defineRadical(
  ['缶'],
  { radicalMeaningDe: 'Krug, Amphore', radicalMeaningZh: 'fǒu' }
)
defineRadical(
  ['耒'],
  { radicalMeaningDe: 'Pflug, Deichsel', radicalMeaningZh: 'lěi' }
)
defineRadical(
  ['舌'],
  { radicalMeaningDe: 'Zunge', radicalMeaningZh: 'shé' }
)
defineRadical(
  ['⺮', '竹'],
  { radicalMeaningDe: 'Bambus', radicalMeaningZh: 'zhú' }
)
defineRadical(
  ['臼'],
  { radicalMeaningDe: 'Mörser', radicalMeaningZh: 'jiù' }
)
defineRadical(
  ['自'],
  { radicalMeaningDe: 'kleine Nase, selbst', radicalMeaningZh: 'zì' }
)
defineRadical(
  ['血'],
  { radicalMeaningDe: 'Blut', radicalMeaningZh: 'xuè' }
)
defineRadical(
  ['舟'],
  { radicalMeaningDe: 'Boot, Kahn', radicalMeaningZh: 'zhōu' }
)
defineRadical(
  ['羽'],
  { radicalMeaningDe: 'Flügel', radicalMeaningZh: 'yǔ' }
)
defineRadical(
  ['艮'],
  { radicalMeaningDe: 'stur, aufrichtig', radicalMeaningZh: 'gèn' }
)
defineRadical(
  ['訁', '言'],
  { radicalMeaningDe: 'Wort, sprechen', radicalMeaningZh: 'yán' }
)
defineRadical(
  ['辛'],
  { radicalMeaningDe: 'bitter, scharf, mühsam', radicalMeaningZh: 'xīn' }
)
defineRadical(
  ['辰'],
  {
    radicalMeaningDe: 'Morgendämmerung, 5. Erdzweig',
    radicalMeaningZh: 'chén'
  }
)
defineRadical(
  ['麦'],
  { radicalMeaningDe: 'Weizen, Getreide', radicalMeaningZh: 'mài' }
)
defineRadical(
  ['走', '赱'],
  { radicalMeaningDe: 'gehen', radicalMeaningZh: 'zǒu' }
)
defineRadical(
  ['赤'],
  {
    radicalMeaningDe: 'zinnoberrot, rot, treu, nackt',
    radicalMeaningZh: 'chì'
  }
)
defineRadical(
  ['豆'],
  { radicalMeaningDe: 'Bohne', radicalMeaningZh: 'dòu' }
)
defineRadical(
  ['束'],
  { radicalMeaningDe: 'Bündel, schnüren', radicalMeaningZh: 'shù' }
)
defineRadical(
  ['酉'],
  { radicalMeaningDe: 'Wein, Alkohol', radicalMeaningZh: 'yǒu' }
)
defineRadical(
  ['豕'],
  { radicalMeaningDe: 'Schwein', radicalMeaningZh: 'shǐ' }
)
defineRadical(
  ['里'],
  { radicalMeaningDe: 'Dorf, Meile, innen', radicalMeaningZh: 'lǐ' }
)
defineRadical(
  ['足', '⻊'],
  { radicalMeaningDe: 'Fuß', radicalMeaningZh: 'zú' }
)
defineRadical(
  ['采'],
  { radicalMeaningDe: 'pflücken, abnehmen', radicalMeaningZh: 'cǎi' }
)
defineRadical(
  ['豸'],
  { radicalMeaningDe: 'Schlange, Wurm', radicalMeaningZh: 'zhì' }
)
defineRadical(
  ['谷'],
  { radicalMeaningDe: 'Tal, Schlucht', radicalMeaningZh: 'gǔ' }
)
defineRadical(
  ['身'],
  { radicalMeaningDe: 'Rumpf, Körper', radicalMeaningZh: 'shēn' }
)
defineRadical(
  ['角'],
  { radicalMeaningDe: 'Horn', radicalMeaningZh: 'jiǎo' }
)
defineRadical(
  ['青', '靑'],
  { radicalMeaningDe: 'blaugrün', radicalMeaningZh: 'qīng' }
)
defineRadical(
  ['龺'],
  { radicalMeaningDe: '', radicalMeaningZh: '' }
)
defineRadical(
  ['雨'],
  { radicalMeaningDe: 'Regen', radicalMeaningZh: 'yǔ' }
)
defineRadical(
  ['非'],
  {
    radicalMeaningDe: 'falsch, Fehler, Unrecht, nicht',
    radicalMeaningZh: 'fēi'
  }
)
defineRadical(
  ['齿', '齒'],
  { radicalMeaningDe: 'Backenzahn', radicalMeaningZh: 'chǐ' }
)
defineRadical(
  ['黾'],
  { radicalMeaningDe: 'Kröte, Schildkröte', radicalMeaningZh: 'mǐn' }
)
defineRadical(
  ['隹'],
  { radicalMeaningDe: 'kleiner Vogel', radicalMeaningZh: 'zhuī' }
)
defineRadical(
  ['金', '釒'],
  { radicalMeaningDe: 'Gold, Metall', radicalMeaningZh: 'jīn' }
)
defineRadical(
  ['鱼', '魚'],
  { radicalMeaningDe: 'Fisch', radicalMeaningZh: 'yú' }
)
defineRadical(
  ['音'],
  { radicalMeaningDe: 'Ton, Laut', radicalMeaningZh: 'yīn' }
)
defineRadical(
  ['革'],
  { radicalMeaningDe: 'Leder, verändern', radicalMeaningZh: 'gé' }
)
defineRadical(
  ['是'],
  { radicalMeaningDe: 'sein, richtig, korrekt', radicalMeaningZh: 'shì' }
)
defineRadical(
  ['骨'],
  { radicalMeaningDe: 'Knochen', radicalMeaningZh: 'gǔ' }
)
defineRadical(
  ['香'],
  { radicalMeaningDe: 'Duft, wohlriechend', radicalMeaningZh: 'xiāng' }
)
defineRadical(
  ['鬼'],
  { radicalMeaningDe: 'Gespenst, Geist', radicalMeaningZh: 'guǐ' }
)
defineRadical(
  ['食', '飠'],
  { radicalMeaningDe: 'Nahrung, Speise', radicalMeaningZh: 'shí' }
)
defineRadical(
  ['高', '髙'],
  { radicalMeaningDe: 'hoch', radicalMeaningZh: 'gāo' }
)
defineRadical(
  ['鬲'],
  { radicalMeaningDe: 'Kessel, Dreifuß', radicalMeaningZh: 'lì' }
)
defineRadical(
  ['髟'],
  { radicalMeaningDe: 'Haar', radicalMeaningZh: 'biāo' }
)
defineRadical(
  ['麻'],
  { radicalMeaningDe: 'Hanf', radicalMeaningZh: 'má' }
)
defineRadical(
  ['鹿'],
  { radicalMeaningDe: 'Hirsch', radicalMeaningZh: 'lù' }
)
defineRadical(
  ['黑'],
  { radicalMeaningDe: 'schwarz', radicalMeaningZh: 'hēi' }
)
defineRadical(
  ['鼓', '鼔'],
  { radicalMeaningDe: 'Trommel', radicalMeaningZh: 'gǔ' }
)
defineRadical(
  ['鼠'],
  { radicalMeaningDe: 'Maus, Ratte', radicalMeaningZh: 'shǔ' }
)
defineRadical(
  ['鼻'],
  { radicalMeaningDe: 'Nase', radicalMeaningZh: 'bí' }
)
defineRadical(
  ['毋', '毌', '母'],
  { radicalMeaningDe: 'Mutter', radicalMeaningZh: 'wú' }
)
defineRadical(
  ['干'],
  { radicalMeaningDe: 'trocken', radicalMeaningZh: 'gān' }
)
defineRadical(
  ['生'],
  { radicalMeaningDe: 'Leben', radicalMeaningZh: 'shēng' }
)
defineRadical(
  ['亅'],
  { radicalMeaningDe: 'Haken', radicalMeaningZh: 'jué' }
)
