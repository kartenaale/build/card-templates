import { error } from './debug/log'
import { init as initComponents } from './init'

const front = document.querySelector('.exercise.front')
if (front === null) {
  throw new Error('front not found')
}
if (front.parentElement?.classList?.contains('front-side-on-back') !== true) {
  // when this is part of the back template, leave initialization to the back
  // script when all of the DOM is ready
  void initComponents(front as HTMLElement).then(
    undefined,
    err => { error(JSON.stringify(err)) }
  )
}
