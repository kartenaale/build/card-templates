import { getDataUnsafe } from "./data";

export async function loadText(filename: string): Promise<string> {
  const data = await getDataUnsafe({ path: filename + '.js', key: filename })
  if (typeof data !== 'string') {
    throw new Error(`${filename} loaded, but is not text`)
  }
  return toHtml(data)
}

function toHtml(raw: string): string {
  return raw.split(/[\n-]{2,}/).map(p => `<p>${
    p.replace(/([a-z]+:\/\/[/a-zA-Z-.]+[/a-zA-Z])/g, '<a href="$1">$1</a>')
  }</p>`).join('')
}
