import { loadText } from "./load-text"

export async function initNotice(within: HTMLElement) {
  within.querySelectorAll('[notice-file]').forEach(
    async el => {
      const noticeFile = el.getAttribute('notice-file') ?? ''
      if (noticeFile !== '') {
        el.innerHTML = await loadText(noticeFile)
      }
    }
  )
}
