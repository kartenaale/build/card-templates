import cnchar from 'cnchar'
import cncharOrder from 'cnchar-order'
import cncharRadical from 'cnchar-radical'
import cncharTrad from 'cnchar-trad'
import { isKangxiOrCjkRadical } from './blocks/index'
import { queryPatchedHanziWriterData } from './patched-hanzi-writer'
import { radicalOverrides } from './radical-overrides'

cnchar.use(cncharOrder, cncharRadical, cncharTrad)

interface RadicalInfo {
  radicalInline: string
  radicalCount: number
}

/**
 * CJK unified ideographs that are used as radicals, but are not recognized by
 * cnchar as radicals.
 */
const nonCncharRadicals = new Set<string>([
  '丬',
  '乀',
  '乁',
  '乛',
  '仇',
  '併',
  '刺',
  '剑',
  '叉',
  '嚐',
  '尣',
  '崚',
  '巜',
  '币',
  '弹',
  '彙',
  '戸',
  '曆',
  '杀',
  '枪',
  '棍',
  '棒',
  '歺',
  '死',
  '⺟',
  '毒',
  '氺',
  '淒',
  '淫',
  '炮',
  '炸',
  '爆',
  '⺩',
  '⺪',
  '癒',
  '瞇',
  '砲',
  '穫',
  '⺮',
  '箭',
  '糹',
  '繫',
  '罓',
  '⺳',
  '⺶',
  '舎',
  '艹',
  '衝',
  '襌',
  '訁',
  '赌',
  '赱',
  '⻊',
  '蹟',
  '鍊',
  '钞',
  '隻',
  '颱',
  '髙',
  '髮',
  '鬍',
  '麵',
  '鼔',
  '龵',
  '㔾',
  '㣺'
])

/**
 * @returns radical or undefined if not known
 */
export async function queryRadicalInline (
  char: string
): Promise<string | undefined> {
  const { radicalInline } = await queryRadicalInfo(char)
  if (radicalInline === '') {
    return undefined
  }
  return radicalInline
}

export function isInlineRadical (char: string): boolean {
  return nonCncharRadicals.has(char) ||
    isKangxiOrCjkRadical(char) ||
    // eslint-disable-next-line
    (cnchar as any).radical.isRadical(char) === true
}

export async function queryRadicalInfo (char: string): Promise<RadicalInfo> {
  let radical: string

  const override = radicalOverrides.get(char)
  if (override !== undefined) {
    radical = override
  } else if (isInlineRadical(char)) {
    // already a radical, the radical is the same char
    radical = char
  } else {
    // if not already a radical, ask cnchar, but still use HanziWriter count
    // eslint-disable-next-line
    const cncharData = (cnchar as any).radical(char)[0]
    radical = cncharData.radical
  }

  // HanziWriter has more SVGs than cnchar has stroke counts, so prefer that
  const radicalCount = (await queryPatchedHanziWriterData(radical))
    .strokes.length

  return { radicalInline: radical, radicalCount }
}
