import { animDataSlice, type AnimData, animDataTranslate } from './anim-data'
import {
  StrokeType
} from '../../src-common/stroke-encodings'
import {
  loadHanziWriterDataWithoutOverridesSync
} from './patched-hanzi-writer'

/** If cnchar does not know the char */
interface CharOverride {
  extraStrokes: number
  radical: string
}

interface TradStrokePatch {
  from: number
  to: number
  replacement: TradStrokePatchReplacement
}

type TradStrokePatchReplacement = AnimData & {
  strokeTypes: StrokeType[]
}

export const tradStrokePatches = initStrokePatches()
export const tradCharOverrides = initCharOverrides()
export const tradRadicalToFull = initRadicalToFull()

function initStrokePatches (): Map<string, TradStrokePatch> {
  const patches = new Map<string, TradStrokePatch>()
  // heisig gives a version with heng in the traditional version
  // the x scale looks a bit off, but at least it matches the book better
  patches.set(
    '均',
    {
      from: 5,
      to: 7,
      replacement: {
        ...animDataTranslate(
          animDataSlice(
            loadHanziWriterDataWithoutOverridesSync('勻'),
            2,
            4
          ),
          135, -20
        ),
        strokeTypes: [
          StrokeType.HENG,
          StrokeType.HENG
        ]
      }
    }
  )
  addGrassPatches(patches)
  return patches
}

function addGrassPatches (acc: Map<string, TradStrokePatch>): void {
  const grassStrokes: StrokeType[] = [
    StrokeType.SHU,
    StrokeType.HENG,
    StrokeType.SHU,
    StrokeType.HENG
  ]
  const fourStrokeGrass: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('藍'),
      0,
      4
    ),
    strokeTypes: grassStrokes
  }
  const fourStrokeGrassHigher: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('華'), 0, 4
    ),
    strokeTypes: grassStrokes
  }
  const fourStrokeSmallerLower: TradStrokePatchReplacement = {
    ...animDataTranslate(
      animDataSlice(
        loadHanziWriterDataWithoutOverridesSync('權'), 4, 8
      ),
      -120, -157
    ),
    strokeTypes: grassStrokes
  }
  const fourGrassTopLeft: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('驚'), 0, 4
    ),
    strokeTypes: grassStrokes
  }
  const fourGrassTopLeftLarger: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('勸'),
      0, 4
    ),
    strokeTypes: grassStrokes
  }
  const fourGrassTopRightLefter: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('權'), 4, 8
    ),
    strokeTypes: grassStrokes
  }
  const fourGrassTopRight: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('謨'), 7, 11
    ),
    strokeTypes: grassStrokes
  }
  const threeGrassOnTop = [
    { hanzi: '艹', replacement: fourStrokeGrass },
    { hanzi: '草', replacement: fourStrokeGrass },
    { hanzi: '若', replacement: fourStrokeGrass },
    { hanzi: '花', replacement: fourStrokeGrass },
    { hanzi: '苦', replacement: fourStrokeGrass },
    { hanzi: '莫', replacement: fourStrokeGrassHigher },
    { hanzi: '苗', replacement: fourStrokeGrass },
    { hanzi: '墓', replacement: fourStrokeGrassHigher },
    { hanzi: '萌', replacement: fourStrokeGrass },
    { hanzi: '苛', replacement: fourStrokeGrass },
    { hanzi: '暮', replacement: fourStrokeGrassHigher },
    { hanzi: '茶', replacement: animDataTranslate(fourStrokeGrass, -20, 0) },
    { hanzi: '落', replacement: fourStrokeGrass },
    { hanzi: '茂', replacement: fourStrokeGrass },
    { hanzi: '蔑', replacement: fourStrokeGrassHigher },
    { hanzi: '幕', replacement: fourStrokeGrassHigher },
    { hanzi: '警', replacement: fourGrassTopLeft },
    { hanzi: '董', replacement: animDataTranslate(fourStrokeGrass, -20, 20) },
    { hanzi: '蔴', replacement: fourStrokeGrassHigher },
    { hanzi: '苟', replacement: fourStrokeGrass },
    { hanzi: '敬', replacement: animDataTranslate(fourGrassTopLeftLarger, -50, -50) }
  ]
  threeGrassOnTop.reduce(
    (acc, { hanzi, replacement }) => {
      if (acc.has(hanzi)) {
        throw new Error(`Whoopsie, ${hanzi} is already overridden`)
      }
      acc.set(
        hanzi,
        { from: 0, to: 3, replacement }
      )
      return acc
    },
    acc
  )

  const withThreeGrass = [
    {
      hanzi: '塔',
      from: 3,
      replacement: animDataTranslate(
        fourGrassTopRight,
        -100, -15
      )
    },
    { hanzi: '懂', from: 3, replacement: fourGrassTopRightLefter },
    // TODO find a way to move the grass radical down here
    { hanzi: '寞', from: 3, replacement: fourStrokeSmallerLower }
  ]
  withThreeGrass.reduce(
    (acc, { hanzi, from, replacement }) => {
      if (acc.has(hanzi)) {
        throw new Error(`Whoopsie, ${hanzi} is already overridden`)
      }
      acc.set(
        hanzi,
        {
          from,
          to: from + 3,
          replacement
        }
      )
      return acc
    },
    acc
  )
  const withSeedlingThreeRight = [
    { hanzi: '瞄', from: 5 },
    { hanzi: '猫', from: 3 }
  ]
  const seedlingRight: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('錨'),
      8, 17
    ),
    strokeTypes: [
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG
    ]
  }
  withSeedlingThreeRight.reduce(
    (acc, { hanzi, from }) => {
      if (acc.has(hanzi)) {
        throw new Error(`Whoopsie, ${hanzi} is already overridden`)
      }
      acc.set(
        hanzi,
        {
          from,
          to: from + seedlingRight.strokes.length - 1,
          replacement: seedlingRight
        }
      )
      return acc
    },
    acc
  )
  const withThreeNobodyRight = [
    { hanzi: '漠', from: 3 },
    { hanzi: '模', from: 4 },
    { hanzi: '膜', from: 4 },
    { hanzi: '貘', from: 7 }
  ]
  const fourNobodyRight: TradStrokePatchReplacement = {
    ...animDataSlice(
      loadHanziWriterDataWithoutOverridesSync('謨'),
      7, 18
    ),
    strokeTypes: [
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.NA
    ]
  }
  withThreeNobodyRight.reduce(
    (acc, { hanzi, from }) => {
      if (acc.has(hanzi)) {
        throw new Error(`Whoopsie, ${hanzi} is already overridden`)
      }
      acc.set(
        hanzi,
        {
          from,
          to: from + fourNobodyRight.strokes.length - 1,
          replacement: fourNobodyRight
        }
      )
      return acc
    },
    acc
  )
}

function initCharOverrides (): Map<string, CharOverride> {
  const charOverrides = new Map<string, CharOverride>()
  charOverrides.set('璮', { radical: '玉', extraStrokes: 13 })
  charOverrides.set('矊', { radical: '目', extraStrokes: 14 })
  charOverrides.set('虝', { radical: '虍', extraStrokes: 6 })
  charOverrides.set('複', { radical: '衣', extraStrokes: 9 })
  // cnchar thinks this has ba (8)
  charOverrides.set('義', { radical: '羊', extraStrokes: 7 })
  // no idea why 13 is correct here
  // https://www.mdbg.net/chinese/dictionary
  // ?page=worddict&wdrst=1&wdqb=%E9%9A%A8
  charOverrides.set('隨', { radical: '阜', extraStrokes: 13 })
  // no idea why 13 is correct here
  // https://www.mdbg.net/chinese/dictionary
  // ?page=worddict&wdrst=1&wdqb=%E9%A4%90
  charOverrides.set('餐', { radical: '食', extraStrokes: 7 })
  charOverrides.set('了', { radical: '亅', extraStrokes: 1 })
  return charOverrides
}

/**
 * If cnchar reports a different radical but there is an easy mapping to
 * the correct one.
 *
 * Also normalizes variant forms, see:
 * https://www.xiaoma.info/bushou.php?ext=1
 */
function initRadicalToFull (): Map<string, string> {
  const radicalToFull = new Map()
  radicalToFull.set('艹', '艸')
  radicalToFull.set('王', '玉')
  radicalToFull.set('⺩', '玉')
  radicalToFull.set('忄', '心')
  radicalToFull.set('㣺', '心')
  radicalToFull.set('衤', '衣')
  radicalToFull.set('⺮', '竹')
  radicalToFull.set('糹', '糸')
  radicalToFull.set('纟', '糸')
  radicalToFull.set('攵', '攴')
  radicalToFull.set('刂', '刀')
  radicalToFull.set('丷', '八')
  radicalToFull.set('亻', '人')
  radicalToFull.set('乀', '丿')
  radicalToFull.set('乁', '丿')
  radicalToFull.set('乚', '乙')
  radicalToFull.set('乛', '乙')
  radicalToFull.set('㔾', '卩')
  radicalToFull.set('尣', '尢')
  radicalToFull.set('巛', '川')
  radicalToFull.set('巜', '川')
  radicalToFull.set('彑', '彐')
  radicalToFull.set('户', '戶')
  radicalToFull.set('戸', '戶')
  radicalToFull.set('扌', '手')
  radicalToFull.set('龵', '手')
  radicalToFull.set('歺', '歹')
  radicalToFull.set('⺟', '母')
  radicalToFull.set('毋', '母')
  radicalToFull.set('毋', '母')
  radicalToFull.set('氵', '水')
  radicalToFull.set('氺', '水')
  radicalToFull.set('灬', '火')
  radicalToFull.set('爫', '爪')
  radicalToFull.set('丬', '爿')
  radicalToFull.set('牜', '牛')
  radicalToFull.set('甩', '用')
  radicalToFull.set('⺪', '疋')
  radicalToFull.set('礻', '示')
  radicalToFull.set('罓', '网')
  radicalToFull.set('罒', '网')
  radicalToFull.set('⺳', '网')
  radicalToFull.set('⺶', '羊')
  radicalToFull.set('耂', '老')
  radicalToFull.set('肀', '聿')
  radicalToFull.set('月', '肉') // REVIEW: exceptions?
  radicalToFull.set('襾', '西')
  radicalToFull.set('覀', '西')
  radicalToFull.set('见', '見')
  radicalToFull.set('訁', '言')
  radicalToFull.set('讠', '言')
  radicalToFull.set('贝', '貝')
  radicalToFull.set('赱', '走')
  radicalToFull.set('⻊', '足')
  radicalToFull.set('车', '車')
  radicalToFull.set('辶', '辵')
  radicalToFull.set('釒', '金')
  radicalToFull.set('钅', '金')
  radicalToFull.set('长', '長')
  radicalToFull.set('门', '門')
  radicalToFull.set('靑', '青')
  radicalToFull.set('靣', '面')
  radicalToFull.set('韦', '韋')
  radicalToFull.set('页', '頁')
  radicalToFull.set('风', '風')
  radicalToFull.set('飞', '飛')
  radicalToFull.set('飠', '食')
  radicalToFull.set('饣', '食')
  radicalToFull.set('马', '馬')
  radicalToFull.set('髙', '高')
  radicalToFull.set('鱼', '魚')
  radicalToFull.set('鸟', '鳥')
  radicalToFull.set('鹵', '卤')
  radicalToFull.set('麦', '麥')
  radicalToFull.set('黄', '黃')
  radicalToFull.set('黾', '黽')
  radicalToFull.set('鼔', '鼓')
  radicalToFull.set('齐', '齊')
  radicalToFull.set('齿', '齒')
  radicalToFull.set('龙', '龍')
  radicalToFull.set('龟', '龜')
  return radicalToFull
}
