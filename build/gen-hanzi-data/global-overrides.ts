
export interface AssembleInput {
  char: string
  from: number
  to: number
  deltaRight?: number
  deltaUp?: number,
  dropRadStrokes?: true
}

/**
 * Characters that are not defined but have parts that occur in other
 * characters can be assembled here.
 *
 * In contrast to patched Hanzi Writer, this will also get stroke types from
 * components.
 */
export const globalAssembleFrom: Map<string, Array<AssembleInput>> = new Map()
globalAssembleFrom.set('⻏', [
  {
    char: '都',
    from: 8,
    to: 10,
    dropRadStrokes: true
  }
])
globalAssembleFrom.set('⻞', [
  {
    char: '飯',
    from: 0,
    to: 8,
    dropRadStrokes: true
  }
])
