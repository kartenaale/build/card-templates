import {
  CjkStroke,
  BopomofoLetterStroke,
  CjkUnifiedIdeographsStroke,
  type StrokeTypeMaybeUnclean,
  StrokeType,
  CjkRadicalsSupplementStroke
} from '../../src-common/stroke-encodings'

/**
 * Converts strokes from cnchar to their canonical representation in the data
 * files. By normalizing, we only have to maintain one mapping in the lookup
 * table e.g. for the name.
 *
 * @param uncleanStrokes input strokes with unclean variants, not modified
 * @returns new array with non-canonical versions replaced with canonical ones
 */
export function normalizeStrokes (
  uncleanStrokes: StrokeTypeMaybeUnclean[]
): StrokeType[] {
  return uncleanStrokes.map(normalizeStroke)
}

export function normalizeStroke (unclean: StrokeTypeMaybeUnclean): StrokeType {
  switch (unclean) {
    case StrokeType.TI:
    case StrokeType.WANGOU:
    case StrokeType.XIEGOU:
    case StrokeType.WOGOU:
    case StrokeType.SHUWAN:
    case StrokeType.HENGZHEZHE:
    case StrokeType.HENGZHEGOU:
    case StrokeType.HENGPIE:
    case StrokeType.HENGZHEWANGOU:
    case StrokeType.SHUZHEZHEGOU:
    case StrokeType.HENGZHETI:
    case StrokeType.HENGZHEZHEPIE:
    case StrokeType.HENGPIEWANGOU:
    case StrokeType.HENGZHEWAN:
    case StrokeType.HENGZHEZHEZHE:
    case StrokeType.NA:
    case StrokeType.HENG:
    case StrokeType.SHU:
    case StrokeType.PIE:
    case StrokeType.DIAN:
    case StrokeType.HENGZHE:
    case StrokeType.HENGGOU:
    case StrokeType.SHUZHE:
    case StrokeType.SHUTI:
    case StrokeType.SHUGOU:
    case StrokeType.PIEDIAN:
    case StrokeType.PIEZHE:
    case StrokeType.SHUZHEZHE:
    case StrokeType.SHUWANGOU:
    case StrokeType.HENGXIEGOU:
    case StrokeType.HENGZHEZHEZHEGOU:
    case StrokeType.SHUZHEPIE:
      // already clean, use unchanged
      return unclean

    // all others are replaced with the canonical version if recognized.
    // we have some duplicates cases from above here, but they are supposed
    // to ensure here that changes in the canonical version don't affect the
    // logic here too much.
    case CjkStroke.TI:
      return StrokeType.TI
    case CjkStroke.WANGOU:
      return StrokeType.WANGOU
    case CjkStroke.XIEGOU:
      return StrokeType.XIEGOU
    case CjkStroke.WOGOU:
      return StrokeType.WOGOU
    case CjkStroke.SHUWAN:
      return StrokeType.SHUWAN
    case CjkStroke.HENGZHEZHE:
      return StrokeType.HENGZHEZHE
    case CjkStroke.HENGZHEGOU:
      return StrokeType.HENGZHEGOU
    case CjkStroke.HENGPIE:
      return StrokeType.HENGPIE
    case CjkStroke.HENGZHEWANGOU:
      return StrokeType.HENGZHEWANGOU
    case CjkStroke.SHUZHEZHEGOU:
      return StrokeType.SHUZHEZHEGOU
    case CjkStroke.HENGZHETI:
      return StrokeType.HENGZHETI
    case CjkStroke.HENGZHEZHEPIE:
      return StrokeType.HENGZHEZHEPIE
    case CjkStroke.HENGPIEWANGOU:
      return StrokeType.HENGPIEWANGOU
    case CjkStroke.HENGZHEWAN:
      return StrokeType.HENGZHEWAN
    case CjkStroke.HENGZHEZHEZHE:
      return StrokeType.HENGZHEZHEZHE
    case CjkStroke.NA:
      return StrokeType.NA
    case CjkStroke.HENG:
      return StrokeType.HENG
    case CjkStroke.SHU:
      return StrokeType.SHU
    case CjkStroke.PIE:
      return StrokeType.PIE
    case CjkStroke.DIAN:
      return StrokeType.DIAN
    case CjkStroke.HENGZHE:
      return StrokeType.HENGZHE
    case CjkStroke.HENGGOU:
      return StrokeType.HENGGOU
    case CjkStroke.SHUZHE:
      return StrokeType.SHUZHE
    case CjkStroke.SHUTI:
      return StrokeType.SHUTI
    case CjkStroke.SHUGOU:
      return StrokeType.SHUGOU
    case CjkStroke.PIEDIAN:
      return StrokeType.PIEDIAN
    case CjkStroke.PIEZHE:
      return StrokeType.PIEZHE
    case CjkStroke.SHUZHEZHE:
      return StrokeType.SHUZHEZHE
    case CjkStroke.SHUWANGOU:
      return StrokeType.SHUWANGOU
    case CjkStroke.HENGXIEGOU:
      return StrokeType.HENGXIEGOU
    case CjkStroke.HENGZHEZHEZHEGOU:
      return StrokeType.HENGZHEZHEZHEGOU
    case BopomofoLetterStroke.SHUZHEPIE:
      return StrokeType.SHUZHEPIE
    case BopomofoLetterStroke.PIEZHE:
      return StrokeType.PIEZHE
    case CjkUnifiedIdeographsStroke.HENG:
      return StrokeType.HENG
    case CjkUnifiedIdeographsStroke.SHU:
      return StrokeType.SHU
    case CjkUnifiedIdeographsStroke.DIAN:
      return StrokeType.DIAN
    case CjkUnifiedIdeographsStroke.PIE:
      return StrokeType.PIE
    case CjkUnifiedIdeographsStroke.SHUWANGOU:
      return StrokeType.SHUWANGOU
    case CjkUnifiedIdeographsStroke.HENGGOU:
      return StrokeType.HENGGOU
    case CjkUnifiedIdeographsStroke.SHUGOU:
      return StrokeType.SHUGOU
    case CjkUnifiedIdeographsStroke.HENGZHEGOU:
      return StrokeType.HENGZHEGOU
    case CjkUnifiedIdeographsStroke.HENGZHEZHEZHEGOU:
      return StrokeType.HENGZHEZHEZHEGOU
    case CjkUnifiedIdeographsStroke.PIEDIAN:
      return StrokeType.PIEDIAN
    case CjkUnifiedIdeographsStroke.SHUTI:
      return StrokeType.SHUTI
    case CjkUnifiedIdeographsStroke.HENGZHE:
      return StrokeType.HENGZHE
    case CjkUnifiedIdeographsStroke.PIEZHE:
      return StrokeType.PIEZHE
    case CjkUnifiedIdeographsStroke.SHUZHE:
      return StrokeType.SHUZHE
    case CjkRadicalsSupplementStroke.HENGXIEGOU:
      return StrokeType.HENGXIEGOU

    // and if not recognized, we throw an error
    case CjkStroke.UNUSED0:
    case CjkStroke.UNUSED1:
    case CjkStroke.UNUSED2:
    case CjkStroke.UNUSED3:
    case CjkStroke.UNUSED4:
    default:
      throw new Error(`Not a known stroke kind: ${unclean}`)
  }
}
