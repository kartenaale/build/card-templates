import fs from 'fs'
import path, { join } from 'path'
import readline from 'readline'
import { query } from './query'
import { type HanziData } from '../../src-common/hanzi-data'
import { readdir } from 'fs/promises'

export interface Config {
  /**
   * One char per line in every file, build DB for these.
   */
  pools: string[]
  /** Output directory for JSON */
  out: string
}

export async function generate (config: Config): Promise<void> {
  if (!await isSafeOut(config.out)) {
    throw new Error(
      'output dir should not exist or only contain json files, stop'
    )
  }

  // create temporary out dir
  const generatingDir = `${config.out}-building-${Math.random()}`
  await fs.promises.mkdir(generatingDir)
  try {
    const generated: Array<Promise<void>> = []
    const handled = new Set<string>()
    // build new database in fresh temp dir
    for (const poolFile of config.pools) {
      const poolStream = fs.createReadStream(poolFile)
      const pool = readline.createInterface({
        input: poolStream,
        crlfDelay: Infinity
      })
      for await (const char of pool) {
        if (char.length !== 0 && !handled.has(char)) {
          handled.add(char)
          if (char.length !== 1) {
            const msg =
                `Unexpected line character count: ${char} (${char.length})`
            throw new Error(msg)
          }
          // the underscore ensures that Anki media checks dón't think the file
          // is unused
          const out = join(generatingDir, `_cd${char}.js`)
          generated.push(generateForHanzi(char, out))
        }
      }
    }
    await Promise.all(generated)

    if (fs.existsSync(config.out)) {
      // swap old and new DB if exists (atomically-ish)
      const scratchPath = `${generatingDir}-finished`
      await fs.promises.rename(generatingDir, scratchPath)
      await fs.promises.rename(config.out, generatingDir)
      await fs.promises.rename(scratchPath, config.out)
      // old content deleted in finally
    } else {
      // first time, can move into target (obvious race condition set aside),
      // delete will be without effect
      await fs.promises.rename(generatingDir, config.out)
    }
  } finally {
    // clear either the failed output, or the old content that has moved
    // into its place if successful
    if (fs.existsSync(generatingDir)) {
      await Promise.all(
        (await readdir(generatingDir))
          .map(localPath => path.join(generatingDir, localPath))
          .map(async relPath => { await fs.promises.rm(relPath) })
      )
      await fs.promises.rm(generatingDir, { recursive: true })
    }
  }
}

async function isSafeOut (out: string): Promise<boolean> {
  const outExists = fs.existsSync(out)
  if (outExists) {
    const contents = await fs.promises.readdir(out)
    // if it exists, it is only safe to overwrite if it only contains JSON
    return contents.every(f => f.endsWith('.js'))
  } else {
    const parent = path.dirname(out)
    const parentExists = fs.existsSync(parent)
    return parentExists
  }
}

async function generateForHanzi (
  hanzi: string,
  outJsonPath: string
): Promise<void> {
  const data = formatJsonp(hanzi, await query({ char: hanzi }))
  await fs.promises.writeFile(outJsonPath, data)
}

export function formatJsonp (hanzi: string, data: HanziData): string {
  return `hd('${hanzi}',${JSON.stringify(data)})`
}
