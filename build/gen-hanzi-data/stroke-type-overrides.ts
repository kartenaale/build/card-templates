import { StrokeType } from '../../src-common/stroke-encodings'

/**
 * Custom values for the stroke types, without affecting SVG data.
 * 
 * Can be used to repair broken cnchar entries.
 */
export const strokeTypeOverrides = new Map<string, StrokeType[]>(
  Object.entries({
    艸: [
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.SHU
    ],
    屮: [
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.PIE
    ],
    彐: [
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    癶: [
      StrokeType.HENGPIE,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.NA
    ],
    艹: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU
    ],
    藍: [
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.SHUZHE,
      StrokeType.PIE,
      StrokeType.HENG,

      StrokeType.DIAN,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,

      StrokeType.SHU,
      StrokeType.HENG
    ],
    氵: [
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.TI
    ],
    彳: [
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.SHU
    ],
    耂: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.PIE
    ],
    刂: [
      StrokeType.SHU,
      StrokeType.SHUGOU
    ],
    丶: [StrokeType.DIAN],
    宀: [
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.HENGGOU
    ],
    纟: [
      StrokeType.PIEZHE,
      StrokeType.PIEZHE,
      StrokeType.TI
    ],
    覀: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    夂: [
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    冫: [
      StrokeType.DIAN,
      StrokeType.TI
    ],
    '⼎': [
      StrokeType.DIAN,
      StrokeType.TI
    ],
    灬: [
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN
    ],
    忄: [
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.SHU
    ],
    讠: [
      StrokeType.DIAN,
      StrokeType.HENGZHETI
    ],
    丿: [
      StrokeType.PIE
    ],
    辶: [
      StrokeType.DIAN,
      StrokeType.HENGZHEZHEPIE,
      StrokeType.NA
    ],
    辵: [
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.NA
    ],
    過: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.SHU,
      StrokeType.HENGZHE,

      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.HENGZHEZHEPIE,
      StrokeType.NA
    ],
    // incorrectly (I think?) defined in cnchar with ti for first stroke
    火: [
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.NA
    ],
    扌: [
      StrokeType.HENG,
      StrokeType.SHUGOU,
      StrokeType.TI
    ],
    丷: [
      StrokeType.DIAN,
      StrokeType.PIE
    ],
    疒: [
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.TI
    ],
    '⼀': [
      StrokeType.HENG
    ],
    丨: [
      StrokeType.SHU
    ],
    '⼁': [
      StrokeType.SHU
    ],
    '⼂': [
      StrokeType.DIAN
    ],
    '⼃': [
      StrokeType.PIE
    ],
    乀: [
      StrokeType.NA
    ],
    乁: [
      StrokeType.NA
    ],
    '⼄': [
      StrokeType.HENGXIEGOU
    ],
    亅: [
      StrokeType.SHUGOU
    ],
    '⼅': [
      StrokeType.SHUGOU
    ],
    '⼆': [
      StrokeType.HENG,
      StrokeType.HENG
    ],
    '⼇': [
      StrokeType.DIAN,
      StrokeType.HENG
    ],
    '⼈': [
      StrokeType.PIE,
      StrokeType.NA
    ],
    '⼉': [
      StrokeType.PIE,
      StrokeType.SHUWANGOU
    ],
    '⼏': [
      StrokeType.PIE,
      StrokeType.HENGZHEWANGOU
    ],
    '⼊': [
      StrokeType.PIE,
      StrokeType.NA
    ],
    '⼋': [
      StrokeType.PIE,
      StrokeType.NA
    ],
    '⼌': [
      StrokeType.SHU,
      StrokeType.HENGZHEGOU
    ],
    冖: [
      StrokeType.DIAN,
      StrokeType.HENGGOU
    ],
    '⼍': [
      StrokeType.DIAN,
      StrokeType.HENGGOU
    ],
    '⼐': [
      StrokeType.SHUZHE,
      StrokeType.SHU
    ],
    '⼑': [
      StrokeType.HENGZHEGOU,
      StrokeType.PIE
    ],
    '⼒': [
      StrokeType.HENGZHEGOU,
      StrokeType.PIE
    ],
    '⼓': [
      StrokeType.PIE,
      StrokeType.HENGZHEGOU
    ],
    '⼔': [
      StrokeType.PIE,
      StrokeType.SHUWANGOU
    ],
    匚: [
      StrokeType.HENG,
      StrokeType.SHUZHE
    ],
    '⼕': [
      StrokeType.HENG,
      StrokeType.SHUZHE
    ],
    匸: [
      StrokeType.HENG,
      StrokeType.SHUZHE
    ],
    '⼖': [
      StrokeType.HENG,
      StrokeType.SHUZHE
    ],
    '⼗': [
      StrokeType.HENG,
      StrokeType.SHU
    ],
    '⼘': [
      StrokeType.SHU,
      StrokeType.DIAN
    ],
    '⼙': [
      StrokeType.HENGZHEGOU,
      StrokeType.SHU
    ],
    '⼚': [
      StrokeType.HENG,
      StrokeType.PIE
    ],
    '⼛': [
      StrokeType.PIEZHE,
      StrokeType.DIAN
    ],
    '⼜': [
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    '⼝': [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG
    ],
    '⼞': [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG
    ],
    '⼟': [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    '⼠': [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    '⼡': [
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    夊: [
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    '⼢': [
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    '⼣': [
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.DIAN
    ],
    '⼤': [
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.NA
    ],
    '⼥': [
      StrokeType.PIEDIAN,
      StrokeType.PIE,
      StrokeType.HENG
    ],
    '⼦': [
      StrokeType.HENGPIE,
      StrokeType.SHUGOU,
      StrokeType.HENG
    ],
    '⼧': [
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.HENGGOU
    ],
    '⼨': [
      StrokeType.HENG,
      StrokeType.SHUGOU,
      StrokeType.DIAN
    ],
    '⼩': [
      StrokeType.SHUGOU,
      StrokeType.PIE,
      StrokeType.DIAN
    ],
    '⼪': [
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHUWANGOU
    ],
    '⼫': [
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.PIE
    ],
    '⼾': [
      StrokeType.PIE,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.PIE
    ],
    戸: [
      StrokeType.HENG,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.PIE
    ],
    '⼬': [
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.SHU
    ],
    '⼭': [
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.SHU
    ],
    '⼮': [
      StrokeType.PIEDIAN,
      StrokeType.PIEDIAN,
      StrokeType.PIEDIAN
    ],
    巜: [
      StrokeType.PIEDIAN,
      StrokeType.PIEDIAN
    ],
    '⼯': [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    '⼰': [
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHUWANGOU
    ],
    '⼱': [
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.SHU
    ],
    '⼲': [
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU
    ],
    '⼳': [
      StrokeType.PIEZHE,
      StrokeType.PIEZHE,
      StrokeType.DIAN // unsure
    ],
    '⼴': [
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.PIE
    ],
    廴: [
      StrokeType.HENGZHEZHEPIE,
      StrokeType.NA
    ],
    '⼵': [
      StrokeType.HENGZHEZHEPIE,
      StrokeType.NA
    ],
    '⼶': [
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHU
    ],
    '⼷': [
      StrokeType.HENG,
      StrokeType.XIEGOU,
      StrokeType.DIAN
    ],
    '⼽': [
      StrokeType.HENG,
      StrokeType.XIEGOU,
      StrokeType.PIE,
      StrokeType.DIAN
    ],
    '⼸': [
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHUZHEZHEGOU
    ],
    '⼹': [
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    彡: [
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.PIE
    ],
    '⼺': [
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.PIE
    ],
    '⼻': [
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.SHU
    ],
    '⼼': [
      StrokeType.DIAN,
      StrokeType.WOGOU,
      StrokeType.DIAN,
      StrokeType.DIAN
    ],
    '⼿': [
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHUGOU
    ],
    '⽀': [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    '⽁': [
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    '⽂': [
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.NA
    ],
    '⽃': [
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.SHU
    ],
    '⽄': [
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU
    ],
    '⽅': [
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.HENGZHEGOU,
      StrokeType.PIE
    ],
    亠: [
      StrokeType.DIAN,
      StrokeType.HENG
    ],
    犭: [
      StrokeType.DIAN,
      StrokeType.WANGOU,
      StrokeType.PIE
    ],
    卩: [
      StrokeType.HENGZHEGOU,
      StrokeType.SHU
    ],
    禸: [
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.PIEZHE,
      StrokeType.DIAN
    ],
    襾: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    靑: [
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    丬: [
      StrokeType.DIAN,
      StrokeType.TI,
      StrokeType.SHU
    ],
    乚: [
      StrokeType.SHUWANGOU
    ],
    乛: [
      StrokeType.HENGGOU
    ],
    爫: [
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.PIE
    ],
    牜: [
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.TI
    ],
    '⺩': [
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.TI
    ],
    併: [
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHU
    ],
    嚐: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.HENGGOU,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.PIE,
      StrokeType.SHUWANGOU,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    尣: [
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.PIE,
      StrokeType.SHUWANGOU
    ],
    尷: [
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHUWANGOU,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHUZHE,

      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.DIAN,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    崚: [
      StrokeType.SHU,
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHUWAN,
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    彑: [
      StrokeType.PIEZHE,
      StrokeType.HENGPIE,
      StrokeType.HENG
    ],
    歷: [
      StrokeType.HENG,
      StrokeType.PIE,

      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.DIAN,

      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.DIAN,

      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    歺: [
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.DIAN
    ],
    '⺟': [
      StrokeType.SHUZHE,
      StrokeType.HENGZHEGOU,
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.DIAN
    ],
    氺: [
      StrokeType.SHUGOU,
      StrokeType.NA,
      StrokeType.TI,
      StrokeType.PIE,
      StrokeType.NA
    ],
    璮: [
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.TI,

      StrokeType.DIAN,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.HENG
    ],
    '⺪': [
      StrokeType.HENGPIE,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.TI
    ],
    癒: [
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.TI,

      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHUGOU,

      StrokeType.DIAN,
      StrokeType.WOGOU,
      StrokeType.DIAN,
      StrokeType.DIAN
    ],
    瞇: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.NA,

      StrokeType.DIAN,
      StrokeType.HENGZHEZHEPIE,
      StrokeType.NA
    ],
    矊: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.PIEZHE,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,

      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.SHU
    ],
    砲: [
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.PIE,
      StrokeType.HENGZHEGOU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHUWANGOU
    ],
    礻: [
      StrokeType.DIAN,
      StrokeType.HENGPIE,
      StrokeType.SHU,
      StrokeType.DIAN
    ],
    衤: [
      StrokeType.DIAN,
      StrokeType.HENGPIE,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.DIAN
    ],
    襌: [
      StrokeType.DIAN,
      StrokeType.HENGPIE,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.DIAN,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU
    ],
    穫: [
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.DIAN,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG,

      StrokeType.PIE,
      StrokeType.SHU,

      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,

      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    '⺮': [
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.DIAN
    ],
    籤: [
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.DIAN,

      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.DIAN,

      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.TI,

      StrokeType.XIEGOU,
      StrokeType.PIE,
      StrokeType.DIAN
    ],
    糹: [
      StrokeType.PIEZHE,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN
    ],
    繫: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHUZHE,
      StrokeType.DIAN,

      StrokeType.PIE,
      StrokeType.HENGZHEWANGOU,

      StrokeType.HENGPIE,
      StrokeType.NA,

      StrokeType.PIEZHE,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.SHUGOU,
      StrokeType.PIE,
      StrokeType.DIAN
    ],
    纔: [
      StrokeType.PIEZHE,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,

      StrokeType.PIE,
      StrokeType.HENGGOU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.HENG,
      StrokeType.SHUTI,
      StrokeType.PIE,
      StrokeType.SHUWANGOU,

      StrokeType.PIE,
      StrokeType.HENGGOU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHUWANGOU,
      StrokeType.DIAN
    ],
    罒: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    罓: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.PIE,
      StrokeType.NA
    ],
    '⺳': [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.PIE,
      StrokeType.NA
    ],
    '⺶': [
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIE
    ],
    臺: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.DIAN,
      StrokeType.HENGGOU,

      StrokeType.HENG,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    舎: [
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG
    ],
    虝: [
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENGGOU,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHUWAN,
      StrokeType.PIE,
      StrokeType.SHUWANGOU,
      StrokeType.PIE,
      StrokeType.HENGZHEGOU,
      StrokeType.PIE,
      StrokeType.PIE
    ],
    衝: [
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      // flipped here to match hanzi-writer, will be flipped again later
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.TI,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHUGOU
    ],
    訁: [
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG
    ],
    赱: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.NA
    ],
    蹟: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.TI,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.NA
    ],
    釒: [
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.TI
    ],
    鉤: [
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.TI,

      StrokeType.PIE,
      StrokeType.HENGZHEGOU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG
    ],
    鍊: [
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.TI,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.NA
    ],
    鑑: [
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.TI,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHUZHE,

      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.DIAN,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG
    ],
    閒: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.PIE,
      StrokeType.HENGZHEGOU,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    阝: [
      StrokeType.HENGPIEWANGOU,
      StrokeType.SHU
    ],
    隻: [
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    颱: [
      StrokeType.PIE,
      StrokeType.HENGZHEWANGOU,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.TI,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG
    ],
    飠: [
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHUTI,
      StrokeType.DIAN
    ],
    饣: [
      StrokeType.PIE,
      StrokeType.HENGGOU,
      StrokeType.SHUTI
    ],
    髙: [
      StrokeType.DIAN,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG
    ],
    髮: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.PIE,

      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.DIAN
    ],
    鬍: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIEZHE,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.PIE,
      StrokeType.PIE,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,

      StrokeType.PIE,
      StrokeType.HENGZHEGOU,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    鱷: [
      StrokeType.PIE,
      StrokeType.HENGGOU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.DIAN,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    麵: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.PIE,
      StrokeType.NA,
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.NA,

      StrokeType.HENG,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    鼔: [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.TI,

      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    龵: [
      StrokeType.PIE,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIE
    ],
    㔾: [
      StrokeType.HENGZHEGOU,
      StrokeType.SHUWANGOU
    ],
    㣺: [
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.SHU,
      StrokeType.DIAN
    ],
    '⽱': [
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.SHU,
      StrokeType.TI,
      StrokeType.DIAN
    ],
    心: [
      StrokeType.DIAN,
      StrokeType.WOGOU,
      StrokeType.DIAN,
      StrokeType.DIAN
    ],
    岁: [
      StrokeType.SHU,
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.HENGPIE,
      StrokeType.DIAN
    ],
    出: [
      StrokeType.SHUZHE,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.SHUZHE,
      StrokeType.SHU
    ],
    骨: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENGZHE,
      StrokeType.DIAN,
      StrokeType.HENGGOU,
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.HENG,
      StrokeType.HENG
    ],
    體: [
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.HENGGOU,
      StrokeType.SHU,
      StrokeType.HENGZHEGOU,
      StrokeType.HENG,
      StrokeType.HENG,

      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.SHU,
      StrokeType.HENG,

      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENGZHE,
      StrokeType.HENG,
      StrokeType.DIAN,
      StrokeType.DIAN,
      StrokeType.HENG
    ],
    被: [
      StrokeType.DIAN,
      StrokeType.HENGPIE,
      StrokeType.SHU,
      StrokeType.PIE,
      StrokeType.DIAN,
      StrokeType.HENGGOU,
      StrokeType.PIE,
      StrokeType.SHU,
      StrokeType.HENGPIE,
      StrokeType.NA
    ],
    '⻒': [
      StrokeType.HENG,
      StrokeType.SHU,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.HENG,
      StrokeType.PIEZHE,
      StrokeType.DIAN
    ],
    '⺛': [
      StrokeType.HENG,
      StrokeType.SHUZHE,
      StrokeType.PIE,
      StrokeType.SHUWANGOU
    ]
  })
)
