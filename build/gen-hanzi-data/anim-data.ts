import {
  type HanziWriterData
} from './patched-hanzi-writer'

export type AnimData = Pick<
HanziWriterData,
'strokes' | 'medians' | 'radStrokes'
>

export function animDataSlice<T extends AnimData> (
  char: T,
  fromIncl: number,
  toExcl: number
): T {
  const { strokes, radStrokes, medians } = char
  return {
    ...char,
    strokes: strokes.slice(fromIncl, toExcl),
    medians: medians.slice(fromIncl, toExcl),
    radStrokes: radStrokes?.filter(i => i >= fromIncl && i < toExcl)
      ?.map(i => i - fromIncl)
  }
}

export function animDataConcat<L extends AnimData, R extends AnimData> (
  left: L,
  right: R
): L & R {
  const {
    strokes: leftStrokes,
    radStrokes: leftRadStrokes,
    medians: leftMedians
  } = left
  const {
    strokes: rightStrokes,
    radStrokes: rightRadStrokes,
    medians: rightMedians
  } = right
  return {
    ...left,
    ...right,
    strokes: [...leftStrokes, ...rightStrokes],
    medians: [...leftMedians, ...rightMedians],
    radStrokes:
    [
      ...(
        // drop radical strokes on the left if the right defines any
        (
          leftRadStrokes !== undefined &&
            (rightRadStrokes === undefined || rightRadStrokes.length === 0)
        )
          ? leftRadStrokes
          : []
      ),
      ...(rightRadStrokes ?? []).map(i => i + leftStrokes.length)
    ]
  }
}

export function animDataTranslate<T extends AnimData> (
  data: T,
  deltaRight: number,
  deltaUp: number
): T {
  return {
    ...data,
    strokes: data.strokes.map(s => {
      const translated: string[] = []
      const inTokens = s.split(' ')
      let inTokensIdx = 0
      while (inTokensIdx !== inTokens.length) {
        const cmd = inTokens[inTokensIdx]
        translated.push(cmd)
        if (cmd === 'M' || cmd === 'L') {
          const [x, y] = inTokens
            .slice(inTokensIdx + 1, inTokensIdx + 3)
            .map(parseFloat)
          translated.push(((x + deltaRight) | 0).toString())
          translated.push(((y + deltaUp) | 0).toString())
          inTokensIdx += 3
        } else if (cmd === 'Q') {
          const [x1, y1, x2, y2] = inTokens
            .slice(inTokensIdx + 1, inTokensIdx + 5)
            .map(parseFloat)
          translated.push(((x1 + deltaRight) | 0).toString())
          translated.push(((y1 + deltaUp) | 0).toString())
          translated.push(((x2 + deltaRight) | 0).toString())
          translated.push(((y2 + deltaUp) | 0).toString())
          inTokensIdx += 5
        } else if (cmd === 'C') {
          const [x1, y1, x2, y2, x3, y3] = inTokens
            .slice(inTokensIdx + 1, inTokensIdx + 7)
            .map(parseFloat)
          translated.push(((x1 + deltaRight) | 0).toString())
          translated.push(((y1 + deltaUp) | 0).toString())
          translated.push(((x2 + deltaRight) | 0).toString())
          translated.push(((y2 + deltaUp) | 0).toString())
          translated.push(((x3 + deltaRight) | 0).toString())
          translated.push(((y3 + deltaUp) | 0).toString())
          inTokensIdx += 7
        } else if (cmd === 'Z') {
          inTokensIdx += 1
        } else {
          throw new Error(`What is this ${
            inTokens[inTokensIdx]
          } at ${inTokensIdx} in ${s}`)
        }
      }
      return translated.join(' ')
    }),
    medians: data.medians.map(strokeMedians => strokeMedians.map(([x, y]) => {
      return [
        x + deltaRight,
        y + deltaUp
      ]
    }))
  }
}
