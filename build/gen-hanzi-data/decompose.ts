import hanzi from 'hanzi-tool'

hanzi.start()

export function decompose (char: string, level: number): string[] | undefined {
  const parts = hanzi.decompose(char, level).components as string[]
  if (!Array.isArray(parts) || parts.length === 0) {
    return undefined
  }
  return parts
}
