/**
 * Returns a version of input with each character that has an entry in a map
 * replaced by its value in that map.
 *
 * If multiple maps have a value, the last one takes precedence.
 */
export function mapString (
  input: string,
  ...maps: Array<Map<string, string>>
): string {
  let out = ''
  for (const char of input) {
    out += maps.reduce((acc, map) => map.get(char) ?? acc, char)
  }
  return out
}
