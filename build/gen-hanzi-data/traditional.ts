import cnchar from 'cnchar'
import cncharOrder from 'cnchar-order'
import cncharRadical from 'cnchar-radical'
import cncharTrad from 'cnchar-trad'
import {
  type AnimData,
  animDataConcat,
  animDataSlice
} from './anim-data'
import {
  queryPatchedHanziWriterData,
  type HanziWriterData
} from './patched-hanzi-writer'
import { StrokeType } from '../../src-common/stroke-encodings'
import { type HanziData } from '../../src-common/hanzi-data'
import { decompose } from './decompose'
import {
  tradCharOverrides,
  tradRadicalToFull,
  tradStrokePatches
} from './traditional-overrides'
import { queryStrokeTypes } from './stroke-type'
import { queryRadicalInline } from './radical'

cnchar.use(cncharOrder, cncharRadical, cncharTrad)

export interface TraditionalInfo extends AnimData {
  /**
   * Radical as full character, e.g. distinguishing mound and city.
   *
   * `?` if no data available.
   */
  radical: string
  /**
   * Stroke count of the character, by traditional counting rules,
   * i.e. using the full character form of the radical.
   *
   * `?` if no data available.
   */
  count: string
  /** If we fiddled with the strokes, these are the patched stroke types. */
  strokeTypes?: StrokeType[]
}

const noData = {
  radical: '?',
  count: '?'
}

export async function getTraditionalInfo (
  char: string,
  simplified: HanziData
): Promise<TraditionalInfo> {
  if (char.length !== 1) {
    throw new Error('single chars only')
  }

  const converted = applyStrokePatches(simplified, char)

  const override = tradCharOverrides.get(char)
  if (override !== undefined) {
    const radicalCount = await countStrokesTraditional(override.radical)
    return {
      ...converted,
      radical: override.radical,
      count: `${radicalCount}+${override.extraStrokes}`
    }
  }

  const radicalInline = await queryRadicalInline(char)
  if (radicalInline === undefined) {
    console.error(`Cannot get radical for: ${char}`)
    return { ...converted, ...noData }
  }

  const radical = getRadicalFullForm(char, radicalInline)
  const radicalStrokesInline = await countStrokesTraditional(radicalInline)
  const radicalStrokesFull = await countStrokesTraditional(radical)
  const totalStrokes = await countStrokesTraditional(char)
  const extra = Math.max(0, totalStrokes - radicalStrokesInline)
  return {
    ...converted,
    radical,
    count: `${radicalStrokesFull}+${extra}`
  }
}

function getRadicalFullForm (char: string, radicalInline: string): string {
  if (radicalInline === '阝') {
    // special case: if on the right, it means 邑 (city),
    //               but on the left it means 阜 (mound)
    // we check if the character starts with 𠄎 or ㇌ and if so, assume it means
    // mound
    const strokeTypes = queryStrokeTypes(char)
    if (
      strokeTypes[0] === StrokeType.HENGZHEZHEZHEGOU ||
        strokeTypes[0] === StrokeType.HENGPIEWANGOU
    ) {
      // probably on the left
      return '阜'
    } else {
      // probably on the right
      return '邑'
    }
  }
  return tradRadicalToFull.get(radicalInline) ?? radicalInline
}

/**
 * Gets the total stroke count from cnchar and compensates for Taiwan variants,
 * e.g. one stroke is added for each grass radical which is written with four
 * strokes instead of three in Taiwan.
 */
async function countStrokesTraditional (
  char: string
): Promise<number> {
  // radicals: special cases
  if (char === '艹') {
    // in Taiwan this is written with 4 strokes
    return 4
  }
  if (char === '糹') {
    // not defined in cnchar
    return 6
  }
  // radicals: defined in cnchar
  // eslint-disable-next-line
  if ((cnchar.radical as any).isRadical(char) === true) {
    // eslint-disable-next-line
    return (cnchar.radical as any).getRadicalCount(char) as number
  }

  // stroke counts for non-radical characters: try counting SVG strokes first
  const svgStrokeCount = applyStrokePatches(
    await queryPatchedHanziWriterData(char),
    char
  ).strokes.length
  if (svgStrokeCount !== 0) {
    return svgStrokeCount
  }

  // no SVG available => try if there is cnchar stroke kind data, including
  //                     our custom overrides and assume grass radicals were
  //                     written with three stroke in that data and correct for
  //                     this
  const strokeTypeCount = queryStrokeTypes(char).length
  if (strokeTypeCount !== 0) {
    return strokeTypeCount + fourStrokeGrassCorrection(char)
  }

  return 0
}

function fourStrokeGrassCorrection (char: string): number {
  let correction = 0
  const parts = decompose(char, 2)
  if (Array.isArray(parts)) {
    for (const part of parts) {
      if (part === '艹') {
        correction -= 1
      }
    }
  }
  return correction
}

function applyStrokePatches<
  T extends HanziData | HanziWriterData
> (
  simplified: T,
  char: string
): T {
  const patch = tradStrokePatches.get(char)
  if (patch === undefined) {
    return { ...simplified }
  }

  const before = animDataSlice(simplified, 0, patch.from)
  const after = animDataSlice(
    simplified,
    patch.to,
    simplified.strokes.length
  )
  const patched = animDataConcat(
    animDataConcat(
      before,
      patch.replacement
    ),
    after
  )
  patched.strokeTypes = patch.replacement.strokeTypes.length > 0
    ? patched.strokeTypes.slice(0, patch.from)
      .concat(patch.replacement.strokeTypes)
      .concat(patched.strokeTypes.slice(patch.to))
    : []
  return patched
}
