/**
 * Maps characters to a radical where data from cnchar is incorrect.
 */
export const radicalOverrides = new Map<string, string>()
radicalOverrides.set("现", "⺩") // 王
radicalOverrides.set("現", "⺩") // 王
radicalOverrides.set("棒", "木")
