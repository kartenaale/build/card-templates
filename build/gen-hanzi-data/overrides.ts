/**
 * Sets the given overridden values without changing the arguments and returns
 * the result.
 *
 * If the override contains objects or arrays, they are set verbatim, not
 * merged.
 */
export function applyTrad<
  T extends { trad?: Partial<Omit<T, 'trad'>> }
> (
  dataArg: T
): T {
  const { trad, ...data } = dataArg
  return {
    ...dataArg,
    ...applyOverridesInPlace(data, trad)
  }
}

/**
 * Sets the given overridden values in-place in data and returns data.
 *
 * If the override contains objects or arrays, they are set verbatim, not
 * merged.
 */
export function applyOverridesInPlace<T> (
  data: T,
  override: Partial<T> | undefined
): T {
  if (override !== undefined) {
    for (const prop in override) {
      const value = override[prop]
      if (value === undefined) {
        throw new Error('overrides must not be undefined')
      }
      data[prop] = value
    }
  }
  return data
}
