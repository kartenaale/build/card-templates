const process = require('process')
const serveStatic = require("serve-static")
module.exports = function (app) {
  console.error(('proxy config'))
  if (!('BUILD_PREFIX' in process.env)) {
    throw new Error(`Dunno where the chardata at, what BUILD_PREFIX?`)
  }
  app.use(`/${process.env.BUILD_PREFIX}`, serveStatic(process.env.BUILD_PREFIX))
  console.error(`data at /${process.env.BUILD_PREFIX}`)
}
