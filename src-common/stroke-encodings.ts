/**
 * These stroke kinds may be produced by our libraries.
 *
 * We clean them before generating hanzi data JSONs.
 *
 * See `StrokeType` for the type used in outputs.
 */
export type StrokeTypeMaybeUnclean =
  StrokeType |
  CjkStroke |
  BopomofoLetterStroke |
  CjkUnifiedIdeographsStroke |
  CjkRadicalsSupplementStroke

/**
 * These are actually intended as strokes.
 *
 * Defines all characters in the block.
 *
 * See: https://en.wikipedia.org/wiki/CJK_Strokes_(Unicode_block)
 */
export const enum CjkStroke {
  TI = '㇀',
  WANGOU = '㇁',
  XIEGOU = '㇂',
  WOGOU = '㇃',
  SHUWAN = '㇄',
  HENGZHEZHE = '㇅',
  HENGZHEGOU = '㇆',
  HENGPIE = '㇇',
  HENGZHEWANGOU = '㇈',
  SHUZHEZHEGOU = '㇉',
  HENGZHETI = '㇊',
  HENGZHEZHEPIE = '㇋',
  HENGPIEWANGOU = '㇌',
  HENGZHEWAN = '㇍',
  HENGZHEZHEZHE = '㇎',
  NA = '㇏',
  HENG = '㇐',
  SHU = '㇑',
  UNUSED0 = '㇒',
  PIE = '㇓',
  DIAN = '㇔',
  HENGZHE = '㇕',
  HENGGOU = '㇖',
  SHUZHE = '㇗',
  UNUSED1 = '㇘',
  SHUTI = '㇙',
  SHUGOU = '㇚',
  PIEDIAN = '㇛',
  PIEZHE = '㇜',
  UNUSED2 = '㇝',
  SHUZHEZHE = '㇞',
  SHUWANGOU = '㇟',
  HENGXIEGOU = '㇠',
  HENGZHEZHEZHEGOU = '㇡',
  UNUSED3 = '㇢',
  UNUSED4 = '㇣'
}

/**
 * Bopomofo letters that look like strokes and are sometimes used for that
 * purpose.
 */
export const enum BopomofoLetterStroke {
  SHUZHEPIE = 'ㄣ',
  PIEZHE = 'ㄥ',
}

/**
 * Letters in the `CJK Unified Ideographs, U+4E00 - U+9FFF` block that look
 * like strokes.
 */
export const enum CjkUnifiedIdeographsStroke {
  HENG = '一',
  SHU = '丨',
  DIAN = '丶',
  PIE = '丿',
  SHUWANGOU = '乚',
  HENGGOU = '乛',
  SHUGOU = '亅',
  HENGZHEGOU = '𠃌',
  HENGZHEZHEZHEGOU = '𠄎',
  PIEDIAN = '𡿨',
  SHUTI = '𠄌',
  HENGZHE = '𠃍',
  PIEZHE = '𠃋',
  SHUZHE = '𠃊'
}

export const enum CjkRadicalsSupplementStroke {
  HENGXIEGOU = '⺄'
}

/**
 * Strokes in our JSONs always have this subset of the other types.
 *
 * Most strokes are in the strokes block, but we also use
 * `BopomofoLetterStroke.SHUZHEPIE`, which does not have a good replacement
 * in the strokes block.
 *
 * If our databases use a different variation during character build, we
 * convert to this representation.
 */
export const enum StrokeType {
  TI = CjkStroke.TI,
  WANGOU = CjkStroke.WANGOU,
  XIEGOU = CjkStroke.XIEGOU,
  WOGOU = CjkStroke.WOGOU,
  SHUWAN = CjkStroke.SHUWAN,
  HENGZHEZHE = CjkStroke.HENGZHEZHE,
  HENGZHEGOU = CjkStroke.HENGZHEGOU,
  HENGPIE = CjkStroke.HENGPIE,
  HENGZHEWANGOU = CjkStroke.HENGZHEWANGOU,
  SHUZHEZHEGOU = CjkStroke.SHUZHEZHEGOU,
  HENGZHETI = CjkStroke.HENGZHETI,
  HENGZHEZHEPIE = CjkStroke.HENGZHEZHEPIE,
  HENGPIEWANGOU = CjkStroke.HENGPIEWANGOU,
  HENGZHEWAN = CjkStroke.HENGZHEWAN,
  HENGZHEZHEZHE = CjkStroke.HENGZHEZHEZHE,
  NA = CjkStroke.NA,
  HENG = CjkStroke.HENG,
  SHU = CjkStroke.SHU,
  PIE = CjkStroke.PIE,
  DIAN = CjkStroke.DIAN,
  HENGZHE = CjkStroke.HENGZHE,
  HENGGOU = CjkStroke.HENGGOU,
  SHUZHE = CjkStroke.SHUZHE,
  SHUTI = CjkStroke.SHUTI,
  SHUGOU = CjkStroke.SHUGOU,
  PIEDIAN = CjkStroke.PIEDIAN,
  PIEZHE = CjkStroke.PIEZHE,
  SHUZHEZHE = CjkStroke.SHUZHEZHE,
  SHUWANGOU = CjkStroke.SHUWANGOU,
  HENGXIEGOU = CjkStroke.HENGXIEGOU,
  HENGZHEZHEZHEGOU = CjkStroke.HENGZHEZHEZHEGOU,
  SHUZHEPIE = BopomofoLetterStroke.SHUZHEPIE,
}
