# Card Templates
The HTML and JS for Anki card templates are developed here.

Part of the templates is animation and other data on many chinese characters.

## What does it look like?
There are different kinds, from simple Q/A cards to more complex cards
specially crafted for learning Chinese or other purposes.

A card for practicing writing, with details opened for the first:

<img src="screenshots/example_molaoshi.png" alt="Writing" width="400">

A simple Q/A card:

<img src="screenshots/example_facts.png" alt="Q/A" width="400">

The design philosophy is for the card templates to be simple first, for
learners, card authors, and developers (in that order). Extra information on
top of the simple view should be just a click away but hidden per default.
For instance, the stroke kinds for the animation shown above are hidden per
default to keep it simple for users. To keep it simple for card authors,
animation data, stroke kinds, radicals etc. are included in the template and
don't need to be filled in manually.

## Key features
* animations for chinese characters (simplified and traditional, offline)
* stroke type tables for chinese characters
* see radicals of characters

## What templates are available and how to use them in an Anki pack?

### bijective
Facts cards, both ways. For `A`, need to know `B`, for `B` need to know `A`.

Example: learn by heart which planet is 4th from the sun, but also learn by
heart what's the planet number of Mars (4th).

#### Fields
* `A`: Question/answer
* `B`: Question/answer
* `Credits`: Give credit to your source here

#### Note types
* `Bijection`: asks for _B_, given _A_;  asks for _A_, given _B_

#### Example
```
content_version: 1.0.0

templates:
- bijective

content:
- import_csv:
    note_type: Bijection
    content_version: 2024-06-08 18:23:00+00:00
    file_patterns:
    - 'content.csv'
    delimiter: ';'
    deck_name_pattern: 'Tapir geography'
    fields_mapping:
    - guid
    - A
    - B
    fields_static: {}
    tags: []

resource_paths: []
```

### facts
Q/A knowledge. `Front` is a question and `Back` is an answer.

#### Fields
* `Front`: Question
* `Back`: Answer
* `Credits`: Give credit to your source here

#### Note types
* `Q/A`: Asks the question

#### Example
```
content_version: 1.0.0

templates:
- facts

content:
- import_csv:
    content_version: 2024-06-08 18:23:00+00:00
    note_type: Facts
    file_patterns:
    - '*.csv'
    delimiter: ';'
    deck_name_pattern: 'Tapir facts'
    fields_mapping:
    - guid
    - Front
    - Back
    fields_static: {}
    tags: []

resource_paths: []
```

### hanzi
Built for Heisig/Richardson books: asks a key word for a character, showing a
frame number. The Hanzi animation is large and optimized for single characters.

#### Fields
* `Keyword`: Key word on the front
* `Keyword type`: e.g. _n._, shown in parentheses after key word
* `Notes (Front)`: Extra section on the front for custom clarifications or notes
* `Hanzi`: Chinese character
* `Book`: 1 or 2 for the book number
* `Lesson`: Lesson number
* `Frame`: Frame number shown on the top
* `Order (Sequential)`: Order if studying book 1 and then book 2
* `Order (Parallel)`: Order if studying book 1 and 2 simultaneously
* `Notes (Back)`: Extra section on the back for custom clarifications or notes
* `Traditional`: If non-empty, use traditional versions of characters
* `Simplified`: Unused
* `Credits`: Give credit to your source here

#### Note types
- `Schreiben`

#### Example
TODO

### vocabsimple
Built for learning Chinese writing, reading, listening, speaking, recognizing
radicals.

Optimized for manual use in Anki.

#### Fields
* `English`
* `简体字`
* `繁體字`
* `Pīnyīn`
* `Examples`
* `Remarks`
* `Remarks (front side)`
* `Audio recording`
* `Lesson`
* `Credits`
* `Source`

### vocabulary
Built for learning Chinese writing, reading, listening, speaking, recognizing
radicals.

Includes advanced features ike feature flags, but less convenient for manual
use in Anki.

#### Fields
* `English`
* `简体字`
* `繁體字`
* `Pīnyīn`
* `Examples`
* `Remarks`
* `Remarks (front side)`
* `Zhuyin`
* `Pronounciation`
* `Audio recording`
* `Lesson`
* `Credits`
* `Enable identify radical (简体字)`
* `Enable identify radical (繁體字)`
* `Enable listen`
* `Enable read (Pīnyīn)`
* `Enable read (简体字)`
* `Enable read (繁體字)`
* `Enable write (简体字)`
* `Enable write (繁體字)`

#### Note types
- `Vocabulary`

#### Example
TODO

### molaoshi
Built for learning Chinese writing, reading, listening, speaking, recognizing
radicals, but optimized for german.

#### Fields
* `Deutsch`: German or other non-Chinese
* `简体字`: Simplified chinese characters
* `繁體字`: Traditional chinese characters
* `Pīnyīn`: Chinese
* `Bemerkungen`: Extra section on the back for custom clarifications or notes
* `Beispiele`: Extra section on the back
* `Lektion`: Unused
* `Bemerkungen (Vorderseite)`: Extra section on the front for custom clarifications or notes
* `Standardaussprache`: Used for TTS e.g. in listening
* `Audioaufnahme`: Unused
* `Radikal finden anlegen`: If non-empty, add a card for recognizing the radical of the simplified character
* `Radikal finden (繁體字) anlegen`: If non-empty, add a card for recognizing the radical of the traditional character
* `Schreiben (简体字) anlegen`: If non-empty, add a card for writing the simplified character down
* `Zhuyin`: Unused
* `Lesen (繁體字) anlegen`: If non-empty, add a card for reading the traditional character down
* `Credits`: Give credit to your source here
* `Schreiben (繁體字) anlegen`: If non-empty, add a card for writing the simplified character down
* `Lesen (Pīnyīn) anlegen`
* `Lesen (简体字) anlegen`
* `Hören anlegen`

#### Note types
* `Vokabeln`

#### Example
TODO